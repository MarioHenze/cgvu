#
# Find GLFW
#
# Try to find GLFW library.
# This module defines the following variables:
# - GLFW_INCLUDE_DIRS
# - GLFW_LIBRARIES
# - GLFW_FOUND
#
# The following variables can be set as arguments for the module.
# - GLFW_ROOT_DIR : Root library directory of GLFW
# - GLFW_USE_STATIC_LIBS : Specifies to use static version of GLFW library (Windows only)
#
# References:
# - https://github.com/progschj/OpenGL-Examples/blob/master/cmake_modules/FindGLFW.cmake
# - https://bitbucket.org/Ident8/cegui-mk2-opengl3-renderer/src/befd47200265/cmake/FindGLFW.cmake
#

# Additional modules
include(FindPackageHandleStandardArgs)

# Handle argument GLFW_ROOT_DIR
if ("${GLFW_ROOT_DIR}" STREQUAL "")
	if("$ENV{GLFW_ROOT_DIR}" STREQUAL "")
		# No hints provided to the Find script
	else()
		set(GLFW_ROOT_DIR "$ENV{GLFW_ROOT_DIR}")
	endif()
endif()

# Do the search
if (WIN32)
	# Find include files
	find_path(
		GLFW_INCLUDE_DIR
		NAMES GLFW/glfw3.h
		PATHS
		${GLFW_ROOT_DIR}/include
		$ENV{PROGRAMFILES}/include
		DOC "The directory where GLFW/glfw3.h resides")

	# Handle argument GLFW_USE_STATIC_LIBS
	if (GLFW_USE_STATIC_LIBS)
		set(GLFW_LIBRARY_NAME glfw3)
	else()
		set(GLFW_LIBRARY_NAME glfw3dll)
	endif()

	# Find library files
	if (CMAKE_SIZEOF_VOID_P EQUAL 8)
		if (EXISTS ${GLFW_INCLUDE_DIR}/../lib64)
			find_library(
				GLFW_LIBRARY
				NAMES ${GLFW_LIBRARY_NAME}
				PATHS
				${GLFW_INCLUDE_DIR}/../lib64
				$ENV{PROGRAMFILES}/lib)
		else()
			find_library(
				GLFW_LIBRARY
				NAMES ${GLFW_LIBRARY_NAME}
				PATHS
				${GLFW_INCLUDE_DIR}/../lib
				$ENV{PROGRAMFILES}/lib)
		endif()
	else()
		if (EXISTS ${GLFW_INCLUDE_DIR}/../lib32)
			find_library(
				GLFW_LIBRARY
				NAMES ${GLFW_LIBRARY_NAME}
				PATHS
				${GLFW_INCLUDE_DIR}/../lib32
				$ENV{PROGRAMFILES}/lib)
		else()
			find_library(
				GLFW_LIBRARY
				NAMES ${GLFW_LIBRARY_NAME}
				PATHS
				${GLFW_INCLUDE_DIR}/../lib
				$ENV{PROGRAMFILES}/lib)
		endif()
	endif()

	unset(GLFW_LIBRARY_NAME)
else()
	# Find include files
	find_path(
		GLFW_INCLUDE_DIR
		NAMES GLFW/glfw3.h
		PATHS
		/usr/include
		/usr/local/include
		/sw/include
		/opt/local/include
		${GLFW_ROOT_DIR}/include
		DOC "The directory where GLFW/glfw3.h resides")

	# Find library file
	if (CMAKE_SIZEOF_VOID_P EQUAL 8)
		find_library(
			GLFW_LIBRARY
			NAMES libglfw.so libglfw.so.3 libglfw.so.3.3
			PATHS
			/usr/lib64
			/usr/lib/x86_64-linux-gnu
			/usr/lib
			/usr/local/lib64
			/usr/local/lib
			/sw/lib
			/opt/local/lib
			${GLFW_ROOT_DIR}/lib
			DOC "The GLFW library")
	else()
		find_library(
			GLFW_LIBRARY
			NAMES libglfw.so libglfw.so.3 libglfw.so.3.3
			PATHS
			/usr/lib32
			/usr/lib/i386-linux-gnu
			/usr/lib
			/usr/local/lib32
			/usr/local/lib
			/sw/lib
			/opt/local/lib
			${GLFW_ROOT_DIR}/lib
			DOC "The GLFW library")
	endif()
endif()

# Handle REQUIRD argument, define *_FOUND variable
find_package_handle_standard_args(GLFW DEFAULT_MSG GLFW_INCLUDE_DIR GLFW_LIBRARY)

# Define GLFW_LIBRARIES and GLFW_INCLUDE_DIRS
if (GLFW_FOUND)
	set(GLFW_LIBRARIES ${OPENGL_LIBRARIES} ${GLFW_LIBRARY})
	set(GLFW_INCLUDE_DIRS ${GLFW_INCLUDE_DIR})
endif()

# Hide some variables
mark_as_advanced(GLFW_INCLUDE_DIR GLFW_LIBRARY)
