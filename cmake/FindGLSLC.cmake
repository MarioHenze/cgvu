#
# Find glslc
#
# Try to find glslc GLSLC compiler
# This module defines 
# - GLSLC_COMMAND
# - GLSLC_FOUND
#
# The following variables can be set as arguments for the module.
# - GRAPHICS_SDK_DIR : Root directory of the graphics API SDK containing glslc
#

# Additional modules
include(FindPackageHandleStandardArgs)

# Helper macro for recursively listing all directories, no files
FUNCTION(LISTDIRTREE result scandir)
	file(GLOB_RECURSE children LIST_DIRECTORIES true "${scandir}/." "${scandir}/*")
	set(dirlist ${scandir})
	foreach(child ${children})
		if(IS_DIRECTORY ${child})
			list(APPEND dirlist ${child})
		endif()
	endforeach()
	set(${result} ${dirlist} PARENT_SCOPE)
ENDFUNCTION()


# Only execute if GLSLC_COMMAND not yet set or, if marked as REQUIRED, not yet found
if("${GLSLC_COMMAND}" STREQUAL "" OR (GLSLC_FIND_REQUIRED AND "${GLSLC_COMMAND}" STREQUAL "GLSLC_COMMAND-NOTFOUND"))
	# Make sure the CMake find... functions will actually do something
	set(GLSLC_COMMAND "" FORCE)
	unset(GLSLC_COMMAND)
	unset(GLSLC_COMMAND CACHE)

	# Handle argument GRAPHICS_SDK_DIR
	if ("${GRAPHICS_SDK_DIR}" STREQUAL "")
		if(NOT "$ENV{VK_SDK_PATH}" STREQUAL "")
			string(REPLACE "\\" "/" GRAPHICS_SDK_DIR "$ENV{VK_SDK_PATH}")
		elseif("$ENV{VULKAN_SDK}" STREQUAL "")
			string(REPLACE "\\" "/" GRAPHICS_SDK_DIR "$ENV{VULKAN_SDK}")
		else()
			# No hints provided to the Find script
			message("FindGLSLC: no graphics SDK could be identified, this find script will most likely fail.")
		endif()
	endif()

	# Do the search
	# - search most likely locations first
	set(GRAPHICS_SDK_DIR_TREE "")
	if(CMAKE_SIZEOF_VOID_P EQUAL 4)
		set(GRAPHICS_SDK_DIR_TREE "${GRAPHICS_SDK_DIR}/bin32" "${GRAPHICS_SDK_DIR}" "${GRAPHICS_SDK_DIR}/bin")
	elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
		set(GRAPHICS_SDK_DIR_TREE "${GRAPHICS_SDK_DIR}/bin64" "${GRAPHICS_SDK_DIR}" "${GRAPHICS_SDK_DIR}/bin")
	else()
		set(GRAPHICS_SDK_DIR_TREE "${GRAPHICS_SDK_DIR}" "${GRAPHICS_SDK_DIR}/bin")
	endif()
	find_program(
		GLSLC_COMMAND NAMES glslc glslc.exe HINTS ${GRAPHICS_SDK_DIR_TREE}
		DOC "Path to the GLSL to SPIR-V compiler"
	)
	# - perform exhaustive search
	if(NOT GLSLC_COMMAND AND IS_DIRECTORY ${GRAPHICS_SDK_DIR})
		message("FindGLSLC: attempting to use ${GRAPHICS_SDK_DIR} as SDK root")
		LISTDIRTREE(GRAPHICS_SDK_DIR_TREE ${GRAPHICS_SDK_DIR})
		find_program(
			GLSLC_COMMAND NAMES glslc glslc.exe HINTS ${GRAPHICS_SDK_DIR_TREE}
			DOC "Path to the GLSL to SPIR-V compiler"
		)
	endif()
else()
	# Supress repeated "not found" messages when GLSLC is not required
	set(GLSLC_FIND_QUIETLY 1)
endif()

# Handle REQUIRD argument, define *_FOUND variable
find_package_handle_standard_args(GLSLC DEFAULT_MSG GLSLC_COMMAND)

# Post-process
if(GLSLC_FOUND)
	mark_as_advanced(GLSLC_COMMAND)
endif()
