
#ifndef __TEXTURE_H__
#define __TEXTURE_H__


//////
//
// Includes
//

// C++ STL
#include <vector>
#include <functional>

// GLM library
#include <glm/glm.hpp>

// Library config
#include "cgvu/libconfig.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Vulkan abstraction main classes
class Context;

// Operational abstractions provided by cgvu::Context
class CommandBuffer;

// Classes in this header
class Texture;
class DepthStencilBuffer;



//////
//
// Enums
//

/** @brief The texel format of an image. */
enum class TexelFormat
{
	RGB_INT8,
	RGB_FLT16,
	RGB_FLT32,
	RGBA_INT8,
	RGBA_FLT16,
	RGBA_FLT32,
	D_INT16,
	D16S8_INT,
	D_INT24,
	D24S8_INT,
	D_FLT32,
	S_INT8,

	RGB = RGB_INT8,
	RGBA = RGBA_INT8
};
/** @brief Shorthand for @ref cgvu::TexelFormat . */
typedef TexelFormat TFmt;

/** @brief The dimensionality of an image. */
enum class ImageDimension
{
	I1D,
	I2D,
	I3D,
	CUBE,
};
/** @brief Shorthand for @ref cgvu::ImageDimension . */
typedef ImageDimension IDim;

/** @brief Indicates the functionality of a @ref cgvu::DepthStencilBuffer . */
enum class DepthStencilType
{
	DEPTH_STENCIL,
	DEPTH_ONLY,
	STENCIL_ONLY
};
/** @brief Shorthand for @ref cgvu::DepthStencilType . */
typedef DepthStencilType DST;



//////
//
// Structs and typedefs
//

/**
 * @brief Struct describing an integer offset in 1 to 3 dimensions for use with images.
 */
union ImageOffset
{
	glm::ivec3 v;
	int x, y, z;

	inline ImageOffset() {}
	inline ImageOffset(int ex) : v(ex, 1, 1) {}
	inline ImageOffset(int ex, int ey) : v(ex, ey, 1) {}
	inline ImageOffset(int ex, int ey, int ez) : v(ex, ey, ez) {}
};

/**
 * @brief Struct describing an integer extend in 1 to 3 dimensions for use with images.
 */
union ImageExtend
{
	glm::uvec3 v;
	unsigned x, y, z;

	inline ImageExtend() {}
	inline ImageExtend(unsigned ex) : v(ex, 1, 1) {}
	inline ImageExtend(unsigned ex, unsigned ey) : v(ex, ey, 1) {}
	inline ImageExtend(unsigned ex, unsigned ey, unsigned ez) : v(ex, ey, ez) {}
};



//////
//
// Class definitions
//

/**
 * @brief
 *		Encapsulates a buffer ment for holding image data (textures, volumes etc.) on the
 *		GPU for fast access within a shader.
 */
class CGVU_API ImageObject : public ManagedObject<ImageObject, Context>
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class CommandBuffer;
	friend class Texture;

	// Explicit inheritance
	using ManagedObject::ManagedObject;
	using ManagedObject::operator=;
	using ManagedObject::operator==;


	////
	// Exported types

	/** @brief Callback functor type for use with @ref #addEnsureAction . */
	typedef std::function<void(void)> EnsureAction;


protected:

	////
	// Object construction / destruction

	/** @brief Constructs the image object in the given context. */
	ImageObject(
		Context &ctx, ImageDimension dims, const unsigned *sizes, TexelFormat texelFmt,
		bool supportReadBack=false
	);


	////
	// Methods

	/** @brief Handles deregistering and resource deallocation. */
	void destroy (void);

	/**
	 * @brief Adds some action to be executed at the end of the next @ref #ensure call.
	 *
	 * @note This action gets executed after the @a next call to @ref #ensure @a only !
	 */
	void addPostEnsureAction (const EnsureAction &action);


public:

	////
	// Interface: ManagedObject

	/**
	 * @brief
	 *		Returns a reference to the managing context (see @ref
	 *		cgvu::ManagedObject::manager ).
	 */
	virtual Context& manager (void) override;


	////
	// Methods

	/**
	 * @brief
	 *		For use by command buffers - tells the image that it was the target of some
	 *		command that might its contents.
	 */
	void notifyWork (Handle fence);

	/**
	 * @brief
	 *		Ensures any pending transfer operations into the image are complete and
	 *		executes registered cleanup actions for the active transfer. When binding the
	 *		image via CGVu's descriptor set update facilities in @ref
	 *		cgvu::ShaderProgram , there is no need to call this explicitely.
	 */
	void ensure (void) const;

	/** @brief Returns internal handle to the underlying image object. */
	Handle handle (void) const;

	/** @brief Returns internal handle to the underlying image view. */
	Handle view (void) const;

	/** @brief Returns the logical size, in bytes, of the image. */
	size_t size (void) const;

	/** @brief Returns the size, in bytes, that the image has on the GPU. */
	size_t sizeOnDevice (void) const;

	/** @brief Returns an internal format ID indicating the low-level texel format. */
	Handle getInternalFormat(void) const;

	/** @brief Returns whether the image texels have a depth component. */
	bool hasDepth (void) const;

	/** @brief Returns whether the image texels have a stencil component. */
	bool hasStencil (void) const;

	/** @brief Returns whether the underlying images support host-side read-back. */
	bool supportsReadback (void) const;
};


/**
 * @brief Encapsulates an image that is supposed to be efficiently sampled by the GPU.
 */
class CGVU_API Texture
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class CommandBuffer;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		The default constructor. If constructed this way, the texture will not be
	 *		usable until a functioning instance is moved in using @link
	 *		#operator=(Texture&&) move assignment @endlink .
	 */
	Texture();

	/** @brief Not copy-constructible. */
	Texture(const Texture &) = delete;

	/** @brief The move constructor. */
	Texture(Texture &&other);

	/** @brief Constructs a texture of the given type in the given context. */
	Texture(
		Context &ctx, ImageDimension dims, const ImageExtend &sizes, TexelFormat texelFmt,
		bool keepStagingBuffer=false
	);

	/** @brief Constructs a texture from the given image file in the given context. */
	Texture(Context &ctx, const std::string &filename, bool keepStagingBuffer=false);

	/** @brief The destructor. */
	virtual ~Texture();


	////
	// Operators

	/** @brief Not copy-assignable. */
	Texture& operator= (const Texture &) = delete;

	/** @brief Move assignment. */
	Texture& operator= (Texture &&other);

	/** @brief References the encapsulated image object. */
	operator const ImageObject& (void) const;


	////
	// Methods

	/**
	 * @brief
	 *		Returns a pointer to the staging area the host CPU can write to, making it
	 *		possible to avoid host-side data duplication while the upload is pending.
	 */
	void* stage (void);

	/** @brief Initiates upload of staged data (see @ref #stage ). */
	void upload (void);

	///** @brief Uploads the given texel data to the image. */
	//void upload (void* data);

	/** @brief Returns the width of the texture. */
	unsigned getWidth (void) const;

	/** @brief Returns the height of the texture. */
	unsigned getHeight (void) const;

	/** @brief Returns the depth of the texture. */
	unsigned getDepth (void) const;

	/** @brief Returns whether the texture is a cube map. */
	unsigned isCubeMap (void) const;

	/** @brief Returns the client-side texel format used for the texture. */
	TexelFormat getTexelFormat (void) const;
};


/**
 * @brief
 *		Encapsulates an image that can be used by a @ref cgvu::Pipeline to do depth and
 *		stencil testing.
 */
class CGVU_API DepthStencilBuffer
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class Pipeline;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		The default constructor. If constructed this way, the depth/stencil buffer
	 *		will not be usable until a functioning instance is moved in using @link
	 *		#operator=(DepthStencilBuffer&&) move assignment @endlink .
	 */
	DepthStencilBuffer();

	/** @brief Not copy-constructible. */
	DepthStencilBuffer(const DepthStencilBuffer &) = delete;

	/** @brief The move constructor. */
	DepthStencilBuffer(DepthStencilBuffer &&other);

	/**
	 * @brief
	 *		Constructs a depth/stencil buffer of the given dimensions in the given
	 *		context.
	 */
	DepthStencilBuffer(Context &ctx, unsigned width, unsigned height,
	                   DepthStencilType type=DST::DEPTH_STENCIL,
	                   bool supportReadBack=false);

	/** @brief The destructor. */
	virtual ~DepthStencilBuffer();


	////
	// Operators

	/** @brief Not copy-assignable. */
	DepthStencilBuffer& operator= (const DepthStencilBuffer &) = delete;

	/** @brief Move assignment. */
	DepthStencilBuffer& operator= (DepthStencilBuffer &&other);


	////
	// Methods

	/**
	 * @brief
	 *		Indicates that the buffer should resize itself to fit the new dimensions. If
	 *		the provided sizes match the buffer's current dimensions, the method will
	 *		return false, otherwise it will return true.
	 */
	bool resize (unsigned newWidth, unsigned newHeight);

	/**
	 * @brief
	 *		Returns the image obejct representing the depth buffer for the given swap ID,
	 *		or for the current swapchain frame according to the context of the buffer
	 *		when passing a negative frame ID.
	 */
	const ImageObject& image (signed swapImageID=-1) const;

	/**
	 * @brief
	 *		Returns the size, in bytes, that a single one of the underlying depth buffer
	 *		images would occupy in host-side memory.
	 */
	size_t size (void) const;

	/** @brief Returns the current width of the buffer. */
	unsigned getWidth (void) const;

	/** @brief Returns the current height of the buffer. */
	unsigned getHeight (void) const;

	/** @brief Returns the client-side texel format used by the buffer. */
	TexelFormat getTexelFormat (void) const;

	/** @brief Returns the size, in bytes, of a single texel in the buffer. */
	unsigned getTexelSize (void) const;

	/** @brief Returns whether the underlying images have a depth component. */
	bool hasDepth (void) const;

	/** @brief Returns whether the underlying images have a stencil component. */
	bool hasStencil (void) const;

	/** @brief Returns whether the underlying images support host-side read-back. */
	bool supportsReadback (void) const;

	/** @brief Accesses the complete buffer contents if readback support is enabled. */
	const unsigned char* access (void) const;

	/**
	 * @brief
	 *		Accesses the buffer contents within the specified region if readback
	 *		support is enabled.
	 */
	const unsigned char* access (const ImageOffset &offset,
	                             const ImageExtend &extend) const;

	/** @brief Accesses the buffer contents if readback support is enabled. */
	void dumpToFile (const std::string &filename) const;
};



//////
//
// Function definitions
//

/** @brief Returns the adress of the texel at the given position in a memory range. */
CGVU_API const void* addressAtPos (
	const void *mem, TexelFormat fmt, const glm::uvec2 &sizes, const glm::uvec2 &pos
);

/** @brief Reads a depth value from the given memory location. */
CGVU_API float readDepth (const void *mem, TexelFormat fmt);

/** @brief Reads a depth value from memory at the given texel position. */
CGVU_API float readDepthAt (
	const void *mem, TexelFormat fmt, const glm::uvec2 &sizes, const glm::uvec2 &pos
);



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __TEXTURE_H__
