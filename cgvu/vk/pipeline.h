
#ifndef __PIPELINE_H__
#define __PIPELINE_H__


//////
//
// Includes
//

// GLM library
#include <glm/glm.hpp>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "cgvu/managed.h"

// Module includes
#include "buffer.h"
#include "shaderprog.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Shader program abstraction
class ShaderProgram;

// Vulkan abstraction main classes
class Context;

// Operational abstractions provided by cgvu::Context
class Pipeline;
class StorageBuffer;
class CommandBuffer;



//////
//
// Enums
//

/**
 * @brief Enumeration of descriptors for all supported attribute data types.
 */
enum class AttribFormatDescriptor
{
	FLOAT32,
	VEC2_FLT32,
	VEC3_FLT32,
	VEC4_FLT32,
	FLOAT64,
	VEC2_FLT64,
	VEC3_FLT64,
	VEC4_FLT64
};
/** @brief Shorthand for @ref cgvu::AttributeFormatDescriptor . */
typedef AttribFormatDescriptor AFmt;

/** @brief Enumeration of supported primitive topologies. */
enum class PrimitiveType
{
	POINT_LIST,
	LINE_LIST,
	LINE_STRIP,
	TRIANGLE_LIST,
	TRIANGLE_STRIP,
	TRIANGLE_FAN,
	LINE_LIST_ADJACENCY,
	LINE_STRIP_ADJACENCY,
	TRIANGLE_LIST_ADJACENCY,
	TRIANGLE_STRIP_ADJACENCY,
	PATCH_LIST
};
/** @brief Shorthand for @ref cgvu::PrimitiveType . */
typedef PrimitiveType PT;

/** @brief Enumeration of possible clear options for depth/stencil attachments. */
enum class DepthStencilClearOps
{
	NONE    = 0,
	DEPTH   = 1,
	STENCIL = 2,
	BOTH    = DEPTH | STENCIL
};
/** @brief Shorthand for @ref cgvu::PrimitiveType . */
typedef DepthStencilClearOps DSclear;

/** @brief Enumeration of possible store options for depth/stencil attachments. */
enum class DepthStencilStoreOps
{
	NONE    = (unsigned)DSclear::NONE,
	DEPTH   = (unsigned)DSclear::DEPTH,
	STENCIL = (unsigned)DSclear::STENCIL,
	BOTH    = DEPTH | STENCIL
};
/** @brief Shorthand for @ref cgvu::PrimitiveType . */
typedef DepthStencilStoreOps DSstore;



//////
//
// Structs and typedefs
//

/**
 * @brief
 *		Struct describing the binding of a single buffer into the data stream. If
 *		attribute data should be fetched from several buffers, @ref
 *		cgvu::Pipeline::setInputBindings should be provided with one such struct for
 *		every buffer that is going to be used later on.
 */
struct BufferBindingDesc
{
	/**
	 * @brief The binding ID that is expected to be bound to a buffer during rendering.
	 */
	unsigned binding;

	/**
	 * @brief The stride between successive elements (i.e. includes alignment padding).
	 */
	unsigned stride;

	/**
	 * @brief Specifies whether the buffer elements advance per-instance or per vertex.
	 */
	bool isInstance;
};
/** @brief Convenience shorthand for an array of @ref cgvu::BufferBindingDesc . */
typedef std::vector<BufferBindingDesc> BufferBindingDescs;

/** @brief Struct describing an attribute in data stream. */
struct AttribDesc
{
	/** @brief The slot ID in the data stream. */
	unsigned location;

	/** @brief The binding ID that the attribute should get its data from. */
	unsigned binding;

	/**
	 * @brief
	 *		The offset from the start of an individual buffer element, indicating where
	 *		inside the element the actual value for the attribute is stored.
	 */
	unsigned offset;

	/** @brief The data format of this attribute. */
	AttribFormatDescriptor format;
};
/** @brief Convenience shorthand for an array of @ref cgvu::BufferAttribDesc . */
typedef std::vector<AttribDesc> AttribDescs;



//////
//
// Class definitions
//

/**
 * @brief Encapsulates a render pass. Makeshift facade, not meant for client use for now.
 */
class CGVU_API RenderPass
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class PresentationSurface;
	friend class Pipeline;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief The default constructor. */
	RenderPass();

	/** @brief Not copy-constructible. */
	RenderPass(const RenderPass &) = delete;

	/** @brief The move constructor. */
	RenderPass(RenderPass &&other);

	/** @brief The destructor. */
	virtual ~RenderPass();


	////
	// Operators

	/** @brief Not copy-assignable. */
	RenderPass& operator= (const RenderPass &) = delete;

	/** @brief Move assignment. */
	RenderPass& operator= (RenderPass &&other);


	////
	// Methods

	/** @brief Returns the internal Vulkan handle of the render pass object. */
	Handle handle (void) const;

	/** @brief Reports whether this render pass uses a depth/stencil attachment. */
	bool hasDepthStencil (void) const;

	/** @brief Returns the depth/stencil buffer attached to the render pass. */
	DepthStencilBuffer& depthStencilBuffer (void);

	/** 
	 * @brief Returns the depth/stencil buffer attached to the render pass (read-only).
	 */
	const DepthStencilBuffer& depthStencilBuffer (void) const;
};


/** @brief Encapsulates graphics pipeline state of a logical Vulkan device. */
class CGVU_API Pipeline : public ManagedObject<Pipeline, Context>
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class CommandBuffer;

	// Explicit inheritance
	using ManagedObject::ManagedObject;
	using ManagedObject::operator=;
	using ManagedObject::operator==;


protected:

	////
	// Object construction / destruction

	/** @brief Constructs the pipeline using the given shader program. */
	Pipeline(const ShaderProgram &shaders);


	////
	// Methods

	/** @brief Performs cleanup operations. */
	void destroy (void);

	/** @brief Called whenever the underlying framebuffer is recreated. */
	void onRenewedFramebuffer (Handle newFB);


public:

	////
	// Interface: ManagedObject

	/**
	 * @brief
	 *		Returns a reference to the managing context (see @ref
	 *		cgvu::ManagedObject::manager ).
	 */
	virtual Context& manager (void) override;


	////
	// Methods

	/** @brief Sets attribute input bindings. */
	void setInputBindings (
		PrimitiveType primitiveType, const AttribDescs &attribs,
		const BufferBindingDescs &bindings
	);

	/**
	 * @brief
	 *		Convenience wrapper for setting the input bindings for a simple, comforming
	 *		vertex struct.
	 */
	template<class Vertex>
	void setInputBindings (PrimitiveType primitiveType)
	{
		setInputBindings(
			primitiveType, Vertex::attributeDescs(), Vertex::bindingDescs()
		);
	}

	/** @brief Enables or disables primitive restart. */
	void enablePrimitiveRestart (bool enabled);

	/** @brief attaches a depth/stencil buffer. */
	void attachDepthStencilBuffer (
		const DepthStencilBuffer &buffer, DepthStencilStoreOps store=DSstore::DEPTH,
		DepthStencilClearOps clear=DSclear::BOTH
	);

	/** @brief Compiles the pipeline state for subsequent use. */
	void build (void);

	/** @brief Indicates whether the pipeline has been built. */
	bool isBuilt (void) const;

	/** @brief Retrieves the internal Vulkan handle encapsulated by this pipeline. */
	Handle handle (void) const;

	/** @brief Retrieves pipeline layout information. */
	Handle getLayout (void) const;

	/** @brief Retrieves the shader program bound to the pipeline. */
	ShaderProgram& shaderProg (void);

	/** @brief Retrieves the shader program bound to the pipeline in read-only mode. */
	const ShaderProgram& shaderProg (void) const;

	/**
	 * @brief Retrieves the render pass associated with the pipeline.
	 *
	 * @note
	 *		The render pass architecture is evolving, so expect huge changes to this API.
	 */
	const RenderPass& renderPass (void) const;

	/**
	 * @brief
	 *		Retrieves the internal Vulkan handle to the framebuffer image bound to the
	 *		end of the pipeline.
	 *
	 * @note
	 *		This is a temporary method that will be removed once proper mechanisms are in
	 *		place!
	 */
	Handle fbi (unsigned swapchainImageID) const;
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __PIPELINE_H__
