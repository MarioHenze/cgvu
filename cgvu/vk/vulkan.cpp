
//////
//
// Includes
//

// C++ STL
#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <unordered_map>
#include <unordered_set>

// GLFW library
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// CGVU includes
#include "error.h"
#include "managed.h"
#include "application.h" // ToDo: remove this once proper windowing module exists
#include "util/utils.h"
#include "log/logging.h"

// Module includes
#include "shaderprog.h"
#include "buffer.h"
#include "vulkan_internals.h"

// Implemented header
#include "vulkan.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;

// Error namespace
using namespace err;



//////
//
// Global state
//

// Anonymous namespace begin
namespace {

// Default score sets for device selection
const VulkanDeviceScoreSet
	ssBestDevice = {
		{DevScore::IS_DEDICATED, 1.5f},
		{DevScore::IS_INTEGRATED, 0.75f},
		{DevScore::IS_SOFTWARE, 0.03125f},
		{DevScore::COMPUTE_CAPABLE, 1.25f},
		{DevScore::PER_EXTENSION, 0.1f},
		{DevScore::PER_QUEUE_FAMILY, 1.0f},
		{DevScore::PER_COMPUTE_QUEUE, 0.25f},
		{DevScore::PER_GRAPHICS_QUEUE, 1.0f},
		{DevScore::DEDICATED_TRANSFER, 1.1f},
		{DevScore::UNIFIED_PRESENTATION, 1.1f},
		{DevScore::SWAPCHAIN_SUITABILITY, 0.125f}
	},
	ssEnergySaferDevice = {
		{DevScore::IS_DEDICATED, 0.125f},
		{DevScore::IS_INTEGRATED, 128.0f},
		{DevScore::IS_SOFTWARE, 0.0009765625f},
		{DevScore::COMPUTE_CAPABLE, 1.25f},
		{DevScore::PER_EXTENSION, 0.1f},
		{DevScore::PER_QUEUE_FAMILY, 1.0f},
		{DevScore::PER_COMPUTE_QUEUE, 0.25f},
		{DevScore::PER_GRAPHICS_QUEUE, 1.0f},
		{DevScore::DEDICATED_TRANSFER, 1.1f},
		{DevScore::UNIFIED_PRESENTATION, 1.1f},
		{DevScore::SWAPCHAIN_SUITABILITY, 0.125f}
	},
	ssSoftwareDevice = {
		{DevScore::IS_DEDICATED, 0.0009765625f},
		{DevScore::IS_INTEGRATED, 0.125f},
		{DevScore::IS_SOFTWARE, 128.0f},
		{DevScore::COMPUTE_CAPABLE, 1.25f},
		{DevScore::PER_EXTENSION, 0.1f},
		{DevScore::PER_QUEUE_FAMILY, 1.0f},
		{DevScore::PER_COMPUTE_QUEUE, 0.25f},
		{DevScore::PER_GRAPHICS_QUEUE, 1.0f},
		{DevScore::DEDICATED_TRANSFER, 1.1f},
		{DevScore::UNIFIED_PRESENTATION, 1.1f},
		{DevScore::SWAPCHAIN_SUITABILITY, 0.125f}
};

// The amount of frames that can be pipelined - more means better GPU utilization at
// the cost of higher latency
#define MAX_PRERENDERED_FRAMES uint32_t(2)

// Anonymous namespace end
}



//////
//
// Helper functions
//

// _stricmp macro
#ifndef _MSC_VER
	#define _stricmp strcasecmp
#endif

// Anonymous namespace begin
namespace {

std::string vulkanErrorToString (VkResult errorCode)
{
	switch (errorCode)
	{
	#define STRINGIFY(r) case r: return #r
		STRINGIFY(VK_SUCCESS);
		STRINGIFY(VK_NOT_READY);
		STRINGIFY(VK_TIMEOUT);
		STRINGIFY(VK_EVENT_SET);
		STRINGIFY(VK_EVENT_RESET);
		STRINGIFY(VK_INCOMPLETE);
		STRINGIFY(VK_ERROR_OUT_OF_HOST_MEMORY);
		STRINGIFY(VK_ERROR_OUT_OF_DEVICE_MEMORY);
		STRINGIFY(VK_ERROR_INITIALIZATION_FAILED);
		STRINGIFY(VK_ERROR_DEVICE_LOST);
		STRINGIFY(VK_ERROR_MEMORY_MAP_FAILED);
		STRINGIFY(VK_ERROR_LAYER_NOT_PRESENT);
		STRINGIFY(VK_ERROR_EXTENSION_NOT_PRESENT);
		STRINGIFY(VK_ERROR_FEATURE_NOT_PRESENT);
		STRINGIFY(VK_ERROR_INCOMPATIBLE_DRIVER);
		STRINGIFY(VK_ERROR_TOO_MANY_OBJECTS);
		STRINGIFY(VK_ERROR_FORMAT_NOT_SUPPORTED);
		STRINGIFY(VK_ERROR_SURFACE_LOST_KHR);
		STRINGIFY(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
		STRINGIFY(VK_SUBOPTIMAL_KHR);
		STRINGIFY(VK_ERROR_OUT_OF_DATE_KHR);
		STRINGIFY(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
		STRINGIFY(VK_ERROR_VALIDATION_FAILED_EXT);
		STRINGIFY(VK_ERROR_INVALID_SHADER_NV);
	#undef STRINGIFY
		default:
			return "VK_UNKNOWN_ERROR";
	}
}

std::string vulkanDeviceTypeToString (VkPhysicalDeviceType type)
{
	switch (type)
	{
	#define STRINGIFY(r) case VK_PHYSICAL_DEVICE_TYPE_ ##r: return "DEVICE_TYPE_" #r
		STRINGIFY(OTHER);
		STRINGIFY(INTEGRATED_GPU);
		STRINGIFY(DISCRETE_GPU);
		STRINGIFY(VIRTUAL_GPU);
		STRINGIFY(CPU);
	#undef STRINGIFY
		default: return "VK_UNKNOWN_DEVICE";
	}
}

// Returns the Vulkan filter type for a given sampler filtering enum
inline VkFilter getFilterType (SamplerFiltering filter)
{
	switch (filter)
	{
		case SFlt::NEAREST:
			return VK_FILTER_NEAREST;
		case SFlt::LINEAR:
			return VK_FILTER_LINEAR;

		default:
			// This should not happen... silently fail
			return VK_FILTER_MAX_ENUM;
	}
}

// Returns the Vulkan sampler address mode for a given sampler repeat enum
inline VkSamplerAddressMode getRepeatMode (SamplerRepeat repeat)
{
	switch (repeat)
	{
		case SRpt::REPEAT:
			return VK_SAMPLER_ADDRESS_MODE_REPEAT;
		case SRpt::MIRROR:
			return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
		case SRpt::CLAMP:
			return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		case SRpt::CLAMP_MIRRORED:
			return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
		case SRpt::BORDER:
			return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;

		default:
			// This should not happen... silently fail
			return VK_SAMPLER_ADDRESS_MODE_MAX_ENUM;
	}
}

bool determineDedicatedTransfer (const VulkanQueue &qf)
{
	return   (qf.props.queueFlags == (VK_QUEUE_TRANSFER_BIT|VK_QUEUE_SPARSE_BINDING_BIT))
	      || (qf.props.queueFlags == VK_QUEUE_TRANSFER_BIT);
}

// Anonymous namespace end
}



//////
//
// Exception implementations
//

////
// cgvu::err::VulkanException

VulkanException::VulkanException(
	unsigned errorCode, const log::EventSource &source
) noexcept
	: Exception(source), pimpl(util::toVoidP(errorCode))
{}

VulkanException::VulkanException(
	unsigned errorCode, log::EventSource &&source
) noexcept
	: Exception(std::move(source)), pimpl(util::toVoidP(errorCode))
{}

std::string VulkanException::getDescription (void) const
{
	std::stringstream desc;
	desc << "Vulkan error: " << util::fromVoidP<VkResult>(pimpl) << std::endl
	     << "  " << vulkanErrorToString(util::fromVoidP<VkResult>(pimpl));
	return std::move(desc.str());
}



//////
//
// Class implementations
//

////
// cgvu::PresentationSurface

struct PresentationSurfaceInfo
{
	VkSurfaceCapabilitiesKHR caps = {};
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> modes;
};

struct PresentationSurface_impl
{
	bool initialized=false, started=false;

	Vulkan &vulkan;
	Context *ctx;

	GLFWwindow *hWnd = nullptr;
	bool minimized = false;

	bool dirty = true;
	VkViewport viewport = {};
	VkSurfaceKHR surface = VK_NULL_HANDLE;
	VkSurfaceCapabilitiesKHR caps = {};
	VkSurfaceFormatKHR fmt = {};

	VkSwapchainKHR swapchain_handle = VK_NULL_HANDLE;
	VkPresentModeKHR swapchain_mode;
	std::vector<VkImage> swapchain;
	std::vector<VkImageView> swapchainViews;

	std::map<RenderPass*, std::vector<VkFramebuffer> > fbs;

	PresentationSurface_impl(Vulkan &vulkan) : vulkan(vulkan)
	{}

	static void onWindowResizing (GLFWwindow *window, int w, int h)
	{
	}

	static void onWindowRefresh (GLFWwindow *window)
	{
	}

	static void onFramebufferResize (GLFWwindow *window, int w, int h)
	{
		auto &_ = *reinterpret_cast<PresentationSurface_impl*>(
			glfwGetWindowUserPointer(window)
		);
		if (w == 0 && h == 0)
			_.minimized = true;
		else
		{
			_.viewport.width = float(w); _.viewport.height = float(h);
			_.dirty = true;
		}
	}

	void handleMinimized (void)
	{
		if (minimized)
		{
			int w, h;
			glfwGetFramebufferSize(hWnd, &w, &h);
			while (w == 0 && h == 0)
			{
				glfwWaitEvents();
				glfwGetFramebufferSize(hWnd, &w, &h);
			}
			viewport.width = float(w); viewport.height = float(h);
			minimized = false;
		}
	}

	void createSwapchainFB (
		std::vector<VkFramebuffer> *out, const RenderPass &renderPass
	)
	{
		unsigned numAttachments = 1;
		const DepthStencilBuffer *ds = nullptr;
		if (renderPass.hasDepthStencil())
		{
			ds = &(renderPass.depthStencilBuffer());
			numAttachments = 2;
		}
		for (unsigned i=0; i<swapchainViews.size(); i++)
		{
			VkImageView attachments[2]; attachments[0] = swapchainViews[i];
			if (ds)
				attachments[1] = ds->image(i).view();
			VkFramebufferCreateInfo fbCreateInfo = {};
			fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			fbCreateInfo.renderPass = renderPass.handle();
			fbCreateInfo.attachmentCount = numAttachments;
			fbCreateInfo.pAttachments = attachments;
			fbCreateInfo.width = uint32_t(viewport.width);
			fbCreateInfo.height = uint32_t(viewport.height);
			fbCreateInfo.layers = 1;
			VkFramebuffer FB_tmp;
			VkResult result = vkCreateFramebuffer(
				ctx->handle(), &fbCreateInfo, nullptr, &FB_tmp
			);
			if (result != VK_SUCCESS)
				throw VulkanException(result, CGVU_LOGMSG(
					"PresentationSurface_impl::createSwapchainFB", "Unable to create"
					" framebuffer object for the given render pass class!"
				));
			out->emplace_back(std::move(FB_tmp));
		}
	}
};
#define PSURFACE_IMPL ((PresentationSurface_impl*)pimpl)
#define PSURFACE_PTR(inst) ((PresentationSurface_impl*)((inst).pimpl))

PresentationSurface::PresentationSurface() : pimpl(nullptr) {}

PresentationSurface::PresentationSurface(Vulkan& vulkan)
	: pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new  PresentationSurface_impl(vulkan);
}

PresentationSurface::PresentationSurface(PresentationSurface &&other) : pimpl(nullptr)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
}

PresentationSurface::~PresentationSurface()
{
	// De-allocate implementation instance
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *PSURFACE_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Context can not be used from here on anymore
			impl.started = false;

			// Swapchain resources
			destroySwapchain();

			// Actual surface
			if (impl.surface)
			{
				vkDestroySurfaceKHR(impl.vulkan.handle(), impl.surface, nullptr);
				impl.surface = VK_NULL_HANDLE;
			}

			impl.initialized = false;
		}

		delete PSURFACE_IMPL;
		pimpl = nullptr;
	}
}

PresentationSurface& PresentationSurface::operator= (PresentationSurface &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

void PresentationSurface::init(Handle window)
{
	// Shortcut for saving one indirection
	auto& impl = *PSURFACE_IMPL;

	// Initialization check
	if (impl.initialized)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"PresentationSurface::init", "Instance is already initialized!"
		));

	// Let GLFW do the actual work
	VkResult result = glfwCreateWindowSurface(
		impl.vulkan.handle(), window, nullptr, &impl.surface
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::init", "Unable to create Vulkan surface for the given"
			" window!"
		));
	impl.initialized = true; // There is something to clean up from here on
	impl.hWnd = window;

	// Set up resize monitoring
	glfwSetWindowUserPointer(window, pimpl);
	glfwSetWindowSizeCallback(window, impl.onWindowResizing);
	glfwSetWindowRefreshCallback(window, impl.onWindowRefresh);
	glfwSetFramebufferSizeCallback(window, impl.onFramebufferResize);
}

void PresentationSurface::createSwapchain (
	Context &ctx, Handle capabilities, Handle format, Handle mode,
	const std::vector<unsigned> &queueSharing
)
{
	// Shortcut for saving one indirection
	auto &impl = *PSURFACE_IMPL;

	// Initialization check
	if (!impl.initialized)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"PresentationSurface::createSwapchain", "Instance is not yet initialized!"
		));

	// Determine current framebuffer size
	uint32_t w = 0, h = 0;
	glfwGetFramebufferSize(impl.hWnd, (int*)&w, (int*)&h);
	if (!(w && h))
		throw GLFWException(CGVU_LOGMSG(
			"PresentationSurface::init", "Unable to query framebuffer size from window!"
		));
	impl.viewport.width = float(w); impl.viewport.height = float(h);
	impl.viewport.maxDepth = 1;

	// Configure swapchain
	VkSurfaceCapabilitiesKHR &caps = *((VkSurfaceCapabilitiesKHR*)capabilities);
	VkSwapchainCreateInfoKHR scCreateInfo = {};
	scCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	scCreateInfo.surface = impl.surface;
	scCreateInfo.minImageCount = std::max(
		std::min(caps.maxImageCount, MAX_PRERENDERED_FRAMES), caps.minImageCount
	);
	if (caps.maxImageCount && scCreateInfo.minImageCount > caps.maxImageCount)
		scCreateInfo.minImageCount = caps.maxImageCount;
	const VkSurfaceFormatKHR& fmt = *((VkSurfaceFormatKHR*)format);
	scCreateInfo.imageFormat = fmt.format;
	scCreateInfo.imageColorSpace = fmt.colorSpace;
	if (caps.currentExtent.width != std::numeric_limits<uint32_t>::max())
		scCreateInfo.imageExtent = caps.currentExtent;
	else
	{
		scCreateInfo.imageExtent.width = util::clamp(
			caps.minImageExtent.width, caps.maxImageExtent.width,
			uint32_t(impl.viewport.width)
		);
		scCreateInfo.imageExtent.height = util::clamp(
			caps.minImageExtent.height, caps.maxImageExtent.height,
			uint32_t(impl.viewport.height)
		);
	}
	scCreateInfo.imageArrayLayers = 1;
	scCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	scCreateInfo.presentMode = (VkPresentModeKHR)(size_t)mode;
	if (queueSharing.size() == 1)
		scCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	else
	{
		scCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		scCreateInfo.queueFamilyIndexCount = (unsigned)queueSharing.size();
		scCreateInfo.pQueueFamilyIndices = queueSharing.data();
	}
	// ToDo: Make user-definable
	if (caps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
		scCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	else
		scCreateInfo.preTransform = caps.currentTransform;
	// ToDo: Make user-definable
	if (caps.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR)
		scCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR;
	else if (  caps.supportedCompositeAlpha
	         & VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR)
		scCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR;
	else
		scCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	scCreateInfo.clipped = VK_TRUE;
	scCreateInfo.oldSwapchain = VK_NULL_HANDLE;
	VkDevice dev = ctx.handle(); impl.ctx = &ctx;
	VkResult result = vkCreateSwapchainKHR(
		dev, &scCreateInfo, nullptr, &(impl.swapchain_handle)
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::createSwapchain", "Unable to create swap chain!"
		));
	// Swapchain creation successful, so store the parameters permanently now
	impl.caps = caps;
	impl.fmt = fmt; impl.swapchain_mode = scCreateInfo.presentMode;

	// Obtain handles to the Vulkan images in the swapchain
	uint32_t count = 0;
	result = vkGetSwapchainImagesKHR(
		dev, impl.swapchain_handle, &count, nullptr
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::createSwapchain",
			"Unable to query number of images in newly created swapchain!"
		));
	impl.swapchain.resize(count);
	result = vkGetSwapchainImagesKHR(
		dev, impl.swapchain_handle, &count, impl.swapchain.data()
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::createSwapchain",
			"Unable to retrieve image handles from newly created swapchain!"
		));

	// Create views on the swapchain images
	impl.swapchainViews.resize(impl.swapchain.size());
	for (unsigned i=0; i<impl.swapchainViews.size(); i++)
	{
		VkImageViewCreateInfo ivCreateInfo = {};
		ivCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		ivCreateInfo.image = impl.swapchain[i];
		ivCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		ivCreateInfo.format = scCreateInfo.imageFormat;
		ivCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		ivCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		ivCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		ivCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		ivCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		ivCreateInfo.subresourceRange.baseMipLevel = 0;
		ivCreateInfo.subresourceRange.levelCount = 1;
		ivCreateInfo.subresourceRange.baseArrayLayer = 0;
		ivCreateInfo.subresourceRange.layerCount = 1;
		result = vkCreateImageView(
			dev, &ivCreateInfo, nullptr, &(impl.swapchainViews[i])
		);
		if (result != VK_SUCCESS)
		{
			std::stringstream msg;
			msg << "Unable to create image view on swapchain sub-image #" << i << '!';
			throw VulkanException(result, CGVU_LOGMSG(
				"PresentationSurface::createSwapchain", msg.str()
			));
		}
	}

	// Can be used for rendering now
	impl.started = true;
	impl.dirty = false;

	// Finally, check if we need to recreate any framebuffers
	for (auto &fb : impl.fbs)
	{
		auto &rp = *fb.first;
		// Resize attachments
		if (rp.hasDepthStencil())
			rp.depthStencilBuffer().resize((unsigned)impl.viewport.width,
			                               (unsigned)impl.viewport.height);
		// Create framebuffers
		std::vector<VkFramebuffer> recreatedFB;
		impl.createSwapchainFB(&recreatedFB, *(fb.first));
		fb.second = std::move(recreatedFB);
	}
}

void PresentationSurface::destroySwapchain (void)
{
	// Shortcut for saving one indirection
	auto &impl = *PSURFACE_IMPL;

	// Framebuffers managed by this surface
	VkDevice dev = impl.ctx->handle();
	for (auto &rp : impl.fbs)
	{
		for (VkFramebuffer &fb : rp.second)
		{
			vkDestroyFramebuffer(dev, fb, nullptr);
			fb = VK_NULL_HANDLE;
		}
		rp.second.clear();
	}

	// Swapchain auxillary objects
	impl.swapchain.clear(); // <- we don't own the references to the VkImage objects,
	                        //    so no explicit cleanup necessary
	for (VkImageView &scView : impl.swapchainViews)
		// We do own the references to the VkImageView objects though...
		if (scView)
		{
			vkDestroyImageView(dev, scView, nullptr);
			scView = VK_NULL_HANDLE;
		}
	impl.swapchainViews.clear();

	// Destroy Vulkan swapchain object
	if (impl.swapchain_handle)
	{
		vkDestroySwapchainKHR(dev, impl.swapchain_handle, nullptr);
		impl.swapchain_handle = VK_NULL_HANDLE;
	}
}

unsigned PresentationSurface::acquire (Handle readySemaphore)
{
	// Shortcut for saving one indirection
	auto &impl = *PSURFACE_IMPL;

	// Acquire image from swapchain
	uint32_t idx;
	impl.handleMinimized();
	VkResult result = vkAcquireNextImageKHR(
		impl.ctx->handle(), impl.swapchain_handle, UINT64_MAX, readySemaphore,
		VK_NULL_HANDLE, &idx
	);
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		impl.dirty = true;
		return -1; // return a non-index
	}
	else if (result == VK_SUBOPTIMAL_KHR)
		impl.dirty = true;
	else if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::acquire", "Unable to obtain an image from the"
			" swapchain!"
		));

	// Done!
	return idx;
}

Handle PresentationSurface::getColorFormat (void) const
{
	return &(PSURFACE_IMPL->fmt);
}

Handle PresentationSurface::getPresentationMode (void) const
{
	return PSURFACE_IMPL->swapchain_mode;
}

bool PresentationSurface::queryCapabilities (Handle capsOut, Handle physicalDevice) const
{
	// Shortcut for saving one indirection
	const auto &impl = *PSURFACE_IMPL;

	// Initialization check
	if (!impl.initialized)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"PresentationSurface::queryCapabilities", "Instance is not yet initialized!"
		));

	// Prepare
	PresentationSurfaceInfo *ret = capsOut;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> modes;
	uint32_t count = 0;

	// Capabilities
	VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
		physicalDevice, impl.surface, &ret->caps
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::queryCapabilities",
			"Unable to query device surface capabilities!"
	));
	// Account for unlimited number of swapchain images (e.g. from software devices)
	if (ret->caps.maxImageCount == 0)
		ret->caps.maxImageCount = std::numeric_limits<int32_t>::max();

	// Formats
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(
		physicalDevice, impl.surface, &count, nullptr
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::queryCapabilities",
			"Unable to query number of supported device surface formats!"
		));
	if (count)
	{
		formats.resize(count);
		result = vkGetPhysicalDeviceSurfaceFormatsKHR(
			physicalDevice, impl.surface, &count, formats.data()
		);
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"PresentationSurface::queryCapabilities",
				"Unable to query supported surface formats of device!"
			));
	}
	else
		// Cannot use this device
		return false;

	// Presentation modes
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(
		physicalDevice, impl.surface, &count, nullptr
	);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"PresentationSurface::queryCapabilities",
			"Unable to query number of supported presentation modes of device!"
		));
	if (count)
	{
		modes.resize(count);
		result = vkGetPhysicalDeviceSurfacePresentModesKHR(
			physicalDevice, impl.surface, &count, modes.data()
		);
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"PresentationSurface::queryCapabilities", "Unable to query supported"
				" presentation modes of device!"
			));

		// Device seems usable
		ret->formats = std::move(formats);
		ret->modes = std::move(modes);
		return true;
	}

	// Cannot use this device
	return false;
}

Handle PresentationSurface::obtainFramebuffer (RenderPass &renderPass)
{
	// Shortcut for saving one indirection
	auto &impl = *PSURFACE_IMPL;

	// Retrieve the appropriate framebuffer
	auto it = impl.fbs.find(&renderPass);
	if (it != impl.fbs.end())
		return &(it->second);

	// None found! Create one...
	std::vector<VkFramebuffer> newFB;
	impl.createSwapchainFB(&newFB, renderPass);
	auto &ret = impl.fbs[&renderPass];
	ret = std::move(newFB);
	return &ret;
}

Handle PresentationSurface::obtainFramebuffer (const RenderPass &renderPass) const
{
	// Shortcut for saving one indirection
	auto &impl = *PSURFACE_IMPL;

	// Retrieve the appropriate framebuffer
	auto it = impl.fbs.find(const_cast<RenderPass*>(&renderPass));
	if (it != impl.fbs.end())
		return &(it->second);

	// None found! Throw up...
	throw err::InvalidOpException(CGVU_LOGMSG(
		"PresentationSurface::obtainFramebuffer", "A framebuffer was requested for a"
		" render pass class that does not yet have a framebuffer created for it, but"
		" none could be created since the surface instance is read-only."
	));
}

Handle PresentationSurface::handle (void)
{
	return PSURFACE_IMPL->surface;
}

Handle PresentationSurface::swapchain (void)
{
	return PSURFACE_IMPL->swapchain_handle;
}

unsigned PresentationSurface::getWidth (void) const
{
	return unsigned(PSURFACE_IMPL->viewport.width);
}

unsigned PresentationSurface::getHeight (void) const
{
	return unsigned(PSURFACE_IMPL->viewport.height);
}

unsigned PresentationSurface::getSwapchainLength (void) const
{
	return (unsigned)PSURFACE_IMPL->swapchain.size();
}

Handle PresentationSurface::getSwapchainImage(unsigned id) const
{
    return PSURFACE_IMPL->swapchain[id];
}

Handle PresentationSurface::getCapabilities(void) const {
	return &(PSURFACE_IMPL->caps);
}

Handle PresentationSurface::getFormat(void) const
{
	return &(PSURFACE_IMPL->fmt);
}

Handle PresentationSurface::getPresentMode(void) const
{
	return PSURFACE_IMPL->swapchain_mode;
}

bool PresentationSurface::dirty (void) const
{
	return PSURFACE_IMPL->dirty;
}


////
// cgvu::Context

struct ScoredSurfaceConfig
{
	VkSurfaceCapabilitiesKHR caps = {};
	VkSurfaceFormatKHR format = {};
	VkPresentModeKHR mode = {};
	float formatScore = 0, modeScore = 0;
};

struct Context_impl
{
	bool initialized = false, started = false;

	Handle hWnd;

	Vulkan &vulkan;

	VulkanDeviceInfo phy;
	VkDevice logical = VK_NULL_HANDLE;
	VulkanDeviceScoreSet scoring;

	std::vector<uint32_t> queueSharingInfo;

	PresentationSurface surface;
	std::vector<VkSemaphore> swapchainSemaphores;
	std::vector<std::vector<VkFence> > swapchainFences;
	std::vector<std::vector<VkSemaphore> > renderSemaphores;
	unsigned swapchain_curImg=-1, swapchain_curFrame=-1,
	         swapchain_lastImg=-1, swapchain_lastFrame;
	std::unordered_set<CommandBuffer*> static_drawBuffs;

	std::unordered_set<Context::EventListener*> eventSubscribers;

	// Managed objects lists
	std::unordered_set<BufferObject, BufferObject::Hash> bufferObjs;
	std::unordered_set<ImageObject, ImageObject::Hash> imageObjs;
	std::unordered_set<CommandBuffer, CommandBuffer::Hash> cmdBufs;
	std::unordered_set<Pipeline, Pipeline::Hash> pipelines;
	std::unordered_set<VkSemaphore> semaphores;
	std::unordered_set<VkFence> fences;
	std::unordered_map<unsigned long long, VkSampler> samplers;

	Context_impl(Vulkan &vulkan, VkPhysicalDevice physicalDevice)
		: vulkan(vulkan), phy(physicalDevice)
	{}

	bool scorePresentationSurface (
		ScoredSurfaceConfig *siOut, const PresentationSurface &surface
	) const
	{
		// Score formats
		PresentationSurfaceInfo psi;
		std::multimap<float, VkSurfaceFormatKHR> sfcFormats;
		std::multimap<float, VkPresentModeKHR> sfcModes;
		if (!surface.queryCapabilities(&psi, phy.handle))
			// Cannot present to this surface
			return false;

		std::vector<VkFormat> prefsFmt = {
			VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R8G8B8A8_UNORM,
			VK_FORMAT_A8B8G8R8_UNORM_PACK32
		}; // ToDo: make this user-definable
		std::vector<VkColorSpaceKHR> prefsCS = {
			VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
		}; // ToDo: make this user-definable
		for (const VkSurfaceFormatKHR &fmt : psi.formats)
		{
			auto itFmt = std::find(prefsFmt.begin(), prefsFmt.end(), fmt.format);
			auto itCS = std::find(prefsCS.begin(), prefsCS.end(), fmt.colorSpace);
			if (itFmt != prefsFmt.end() && itCS != prefsCS.end())
			{
				unsigned fmtPos = (unsigned)std::distance(prefsFmt.begin(), itFmt),
				         CSpos = (unsigned)std::distance(prefsCS.begin(), itCS);
				float
					scoreFmt = 0.25f * (1.0f - float(fmtPos)/float(prefsFmt.size())),
					scoreCS = 0.25f * (1.0f - float(CSpos)/float(prefsCS.size()));
				sfcFormats.emplace(scoreFmt+scoreCS, fmt);
			}
		}
		// - score presentation modes
		std::vector<VkPresentModeKHR> prefsMode = {
			VK_PRESENT_MODE_MAILBOX_KHR, VK_PRESENT_MODE_FIFO_RELAXED_KHR,
			VK_PRESENT_MODE_FIFO_KHR, VK_PRESENT_MODE_IMMEDIATE_KHR
		}; // ToDo: make this user-definable
		for (const VkPresentModeKHR &mode : psi.modes)
		{
			auto it = std::find(prefsMode.begin(), prefsMode.end(), mode);
			if (it != prefsMode.end())
			{
				unsigned pos = (unsigned)std::distance(prefsMode.begin(), it);
				float score = (1.0f - float(pos)/float(prefsMode.size())) * 0.5f;
				sfcModes.emplace(score, mode);
			}
		}
		if (sfcFormats.empty() || sfcModes.empty())
			// Cannot present to this surface
			return false;

		// Commit scoring information
		siOut->caps = psi.caps;
		auto format = sfcFormats.crbegin(); auto mode = sfcModes.crbegin();
		siOut->format = format->second; siOut->formatScore = format->first;
		siOut->mode = mode->second; siOut->modeScore = mode->first;

		// Indicate that we can present to the surface
		return true;
	}
};
#define CONTEXT_IMPL ((Context_impl*)pimpl)
#define CONTEXT_PTR(inst) ((Context_impl*)((inst).pimpl))

Context::Context(Vulkan &vulkan, Handle phyDevice) : pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new Context_impl(vulkan, phyDevice);
}

Context::Context(Context &&other) : pimpl(other.pimpl)
{
	other.pimpl = nullptr;
}

Context::~Context()
{
	// De-allocate implementation instance
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *CONTEXT_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Wait until everything is idle
			vkDeviceWaitIdle(impl.logical);

			// Context can not be used from here on anymore
			impl.started = false;

			// Clean up samplers
			for (auto it : impl.samplers)
				vkDestroySampler(impl.logical, it.second, nullptr);
			impl.samplers.clear();

			// Clean up synchronization objects
			// - release our own references
			impl.swapchainSemaphores.clear();
			impl.swapchainFences.clear();
			// - destroy the objects
			for (auto semaphore : impl.semaphores)
				vkDestroySemaphore(impl.logical, semaphore, nullptr);
			impl.semaphores.clear();
			for (auto fence : impl.fences)
				vkDestroyFence(impl.logical, fence, nullptr);
			impl.fences.clear();

			// Destroy command pools
			for (auto &qf : impl.phy.qf)
			{
				if (qf.pool_transient)
				{
					vkDestroyCommandPool(impl.logical, qf.pool_transient, nullptr);
					qf.pool_transient = VK_NULL_HANDLE;
				}
				if (qf.pool_static)
				{
					vkDestroyCommandPool(impl.logical, qf.pool_static, nullptr);
					qf.pool_transient = VK_NULL_HANDLE;
				}
			}

			// Free surface resources
			impl.surface.destroySwapchain();

			// Destroy logical device context
			if (impl.logical)
			{
				vkDestroyDevice(impl.logical, nullptr);
				impl.logical = VK_NULL_HANDLE;
			}

			impl.initialized = false;
		}

		delete CONTEXT_IMPL;
		pimpl = nullptr;
	}
}

Context& Context::operator= (Context &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

BufferObject::Reference Context::createBuffer (
	size_t size, unsigned usageFlags, unsigned memProps
)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Create the abstraction
	BufferObject newBuf(*this, size, usageFlags, memProps);

	// Commit to list of buffer objects
	BufferObject &createdBuf = // ToDo: see if we can get rid of the const_cast
		const_cast<BufferObject&>(*(impl.bufferObjs.emplace(std::move(newBuf)).first));

	// Done!
	return createdBuf;
}

void Context::deleteObject(BufferObject &buffer)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Remove from list of managed buffer objects
	auto it = impl.bufferObjs.find(buffer);
	if (it == impl.bufferObjs.end())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::deleteObject", "Cannot delete a buffer object that is not managed"
			" by this context!"
		));
	BufferObject &buf = const_cast<BufferObject&>(*it);
	buf.destroy();
	impl.bufferObjs.erase(it);
	buffer.pimpl = nullptr;
}

ImageObject::Reference Context::createImage (
	ImageDimension dims, const unsigned *sizes, TexelFormat texelFmt,
	bool supportReadBack
)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Create the abstraction
	ImageObject newImg(*this, dims, sizes, texelFmt, supportReadBack);

	// Commit to list of buffer objects
	ImageObject &createdImg = // ToDo: see if we can get rid of the const_cast
		const_cast<ImageObject&>(*(impl.imageObjs.emplace(std::move(newImg)).first));

	// Done!
	return createdImg;
}

void Context::deleteObject(ImageObject &image)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Remove from list of managed buffer objects
	auto it = impl.imageObjs.find(image);
	if (it == impl.imageObjs.end())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::deleteObject", "Cannot delete an image object that is not managed"
			" by this context!"
		));
	ImageObject &img = const_cast<ImageObject&>(*it);
	img.destroy();
	impl.imageObjs.erase(it);
	image.pimpl = nullptr;
}

CommandBuffer::Reference Context::createCommandBuffer (TaskClass taskClass, bool transient)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Create the abstraction
	CommandBuffer newBuf(*this, taskClass, transient);

	// Commit to list of command buffers
	CommandBuffer &createdBuf = // ToDo: see if we can get rid of the const_cast
		const_cast<CommandBuffer&>(*(impl.cmdBufs.emplace(std::move(newBuf)).first));
	if (isRenderTask(taskClass))
		if (!transient)
			impl.static_drawBuffs.emplace(&createdBuf);

	// Done!
	return createdBuf;
}

void Context::deleteObject (CommandBuffer &buffer)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Remove from list of managed command buffers
	auto it = impl.cmdBufs.find(buffer);
	if (it == impl.cmdBufs.end())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::deleteObject", "Cannot delete a command buffer that is not"
			" managed by this context!"
		));
	CommandBuffer &cmd = const_cast<CommandBuffer&>(*it);
	if (!(cmd.transient()))
		impl.static_drawBuffs.erase(&cmd);
	cmd.destroy();
	impl.cmdBufs.erase(it);
	buffer.pimpl = nullptr;
}

Pipeline::Reference Context::createPipeline (const ShaderProgram &shaderProg)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	if (!(shaderProg.isBuilt()))
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::createPipeline", "Cannot create a pipeline with a shader program"
			" that has not yet been built and linked!"
		));
	if (shaderProg.context().pimpl != pimpl)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::createPipeline", "Cannot create a pipeline with a shader program"
			" that has been built in a different context!"
		));

	// Create the abstraction
	Pipeline newPipeline(shaderProg);

	// Commit to list of pipelines
	Pipeline &createdPipeline = // ToDo: see if we can get rid of the const_cast
		const_cast<Pipeline&>(*(impl.pipelines.emplace(std::move(newPipeline)).first));

	// Done!
	return createdPipeline;
}

void Context::deleteObject (Pipeline &pipeline)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Remove from list of managed pipelines
	auto it = impl.pipelines.find(pipeline);
	if (it == impl.pipelines.end())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::deleteObject", "Cannot delete a pipeline object that is not"
			" managed by this context!"
		));
	Pipeline &pl = const_cast<Pipeline&>(*it);
	pl.destroy();
	impl.pipelines.erase(it);
	pipeline.pimpl = nullptr;
}

Handle Context::createSemaphore (void)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// With current versions of Vulkan, this struct is the same always
	static VkSemaphoreCreateInfo sphCreateInfo = {
		VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0
	};

	// Create the semaphore
	VkSemaphore s;
	VkResult result = vkCreateSemaphore(impl.logical, &sphCreateInfo, nullptr, &s);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Context::createSemaphore", "Unable to create Vulkan semaphore object!"
		));

	// Commit and return
	return *(impl.semaphores.emplace(s).first);
}

void Context::deleteSemaphore (Handle semaphore)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Ignore all delete requests when context not started
	if (!impl.started)
		return;

	// Search in the list of managed semaphores
	auto it = impl.semaphores.find(semaphore);
	if (it == impl.semaphores.end())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::deleteSemaphore", "Cannot delete a semaphore object that is not"
			" managed by this context!"
		));
	impl.semaphores.erase(it);
	vkDestroySemaphore(impl.logical, semaphore, nullptr);
}

Handle Context::createFence (bool signaled)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Fill out create info
	VkFenceCreateInfo fCreateInfo = {
		VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr,
		signaled ? VK_FENCE_CREATE_SIGNALED_BIT : VkFenceCreateFlags(0)
	};

	// Create the semaphore
	VkFence f;
	VkResult result = vkCreateFence(impl.logical, &fCreateInfo, nullptr, &f);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Context::createFence", "Unable to create Vulkan fence object!"
		));

	// Commit and return
	return *(impl.fences.emplace(f).first);
}

void Context::deleteFence (Handle fence)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Ignore all delete requests when context not started
	if (!impl.started)
		return;

	// Search in the list of managed semaphores
	auto it = impl.fences.find(fence);
	if (it == impl.fences.end())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Context::deleteFence", "Cannot delete a fence object that is not managed by"
			" this context!"
		));
	impl.fences.erase(it);
	vkDestroyFence(impl.logical, fence, nullptr);
}

Handle Context::obtainSampler (
	SamplerFiltering minFilter, SamplerFiltering magFilter, SamplerRepeat repeatU,
	SamplerRepeat repeatV, SamplerRepeat repeatW, unsigned anisotropy
)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Sanity check
	if (anisotropy > ((unsigned)impl.phy.props.limits.maxSamplerAnisotropy))
		// None found! Throw up...
		throw NotSupportedException(CGVU_LOGMSG(
			"Context::obtainSampler", "Requested anisotropy exceeds physical device"
			" capabilities!"
		));

	// Hash the parameters
	unsigned long long hash =
		   ((unsigned long long)minFilter)
		| (((unsigned long long)magFilter)<<8)
		| (((unsigned long long)repeatU)<<16)
		| (((unsigned long long)repeatV)<<24)
		| (((unsigned long long)repeatW)<<32)
		| (((unsigned long long)((unsigned short)anisotropy))<<40);

	// Search in the list of managed samplers
	auto it = impl.samplers.find(hash);
	if (it != impl.samplers.end())
		return it->second;

	// Doesn't exist yet, so create it
	VkSamplerCreateInfo samplerCI{};
	samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerCI.magFilter = getFilterType(minFilter);
	samplerCI.minFilter = getFilterType(magFilter);
	samplerCI.addressModeU = getRepeatMode(repeatU);
	samplerCI.addressModeV = getRepeatMode(repeatV);
	samplerCI.addressModeW = getRepeatMode(repeatW);
	samplerCI.anisotropyEnable = anisotropy > 0;
	samplerCI.maxAnisotropy = (float)anisotropy;
	samplerCI.borderColor = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
	samplerCI.unnormalizedCoordinates = VK_FALSE;
	samplerCI.compareEnable = VK_FALSE;
	samplerCI.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerCI.mipLodBias = 0.0f;
	samplerCI.minLod = 0.0f;
	samplerCI.maxLod = 0.0f;
	VkSampler newSampler;
	VkResult result = vkCreateSampler(impl.logical, &samplerCI, nullptr, &newSampler);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Context::obtainSampler", "Unable to create Vulkan sampler object!"
		));

	// Commit and return
	impl.samplers.emplace(hash, newSampler);
	return newSampler;
}

Handle Context::obtainSampler (
	SamplerFiltering minFilter, SamplerFiltering magFilter, SamplerRepeat repeatU,
	SamplerRepeat repeatV, SamplerRepeat repeatW, unsigned anisotropy
) const
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Sanity check
	if (anisotropy > ((unsigned)impl.phy.props.limits.maxSamplerAnisotropy))
		// None found! Throw up...
		throw NotSupportedException(CGVU_LOGMSG(
			"Context::obtainSampler", "Requested anisotropy exceeds physical device"
			" capabilities!"
		));

	// Hash the parameters
	unsigned long long hash =
		   ((unsigned long long)minFilter)
		| (((unsigned long long)magFilter)<<8)
		| (((unsigned long long)repeatU)<<16)
		| (((unsigned long long)repeatV)<<24)
		| (((unsigned long long)repeatW)<<32);

	// Search in the list of managed samplers
	auto it = impl.samplers.find(hash);
	if (it == impl.samplers.end())
		// None found! Throw up...
		throw InvalidOpException(CGVU_LOGMSG(
			"Context::obtainSampler", "A sampler was requested for a parameter set that"
			"did not yet have a sampler, but none could be created since the context"
			"reference is read-only."
		));

	return it->second;
}

void Context::prepareFrame (void)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Wait for currently executing command buffers in the to-be-prepared frame
	if (!(impl.swapchainFences[impl.swapchain_curFrame].empty()))
	{
		VkResult result = vkWaitForFences(
			impl.logical,(uint32_t)impl.swapchainFences[impl.swapchain_curFrame].size(),
			impl.swapchainFences[impl.swapchain_curFrame].data(), VK_TRUE, UINT64_MAX
		);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"Context::prepareFrame", "Unable to synchronize with in-flight command"
				" buffers in current frame!"
			));
		// Clear the corresponding list of rendering-finished fences
		impl.swapchainFences[impl.swapchain_curFrame].clear();
	}

	// Prepare an image for rendering from the swapchain
	unsigned newImgIdx = -1;
	// - loop until image can be acquired
	while (newImgIdx == -1)
	{
		newImgIdx =
			impl.surface.acquire(impl.swapchainSemaphores[impl.swapchain_curFrame]);
		if (newImgIdx == -1)
		{
			// Recreate swapchain
			waitIdle();
			impl.surface.destroySwapchain();
			ScoredSurfaceConfig sc = { };
			if (!impl.scorePresentationSurface(&sc, impl.surface))
				throw err::NotSupportedException(CGVU_LOGMSG(
					"Context::presentFrame", "Device does not support swapchain re-creation"
					" upon surface change!"
				));
			impl.surface.createSwapchain(
				*this, &(sc.caps), &(sc.format), sc.mode, impl.queueSharingInfo
			);
			for (auto sub : impl.eventSubscribers)
				sub->onPresentationSurfaceChanged(*this);
			for (CommandBuffer *buf : impl.static_drawBuffs)
				buf->build();
		}
	}
	// - update image IDs
	impl.swapchain_lastImg = (impl.swapchain_lastImg==-1) ? newImgIdx : impl.swapchain_curImg;
	impl.swapchain_curImg = (newImgIdx != -1) ? newImgIdx : impl.swapchain_curImg;

	// Clear the corresponding list of rendering semaphores
	impl.renderSemaphores[impl.swapchain_curFrame].clear();
}

void Context::submitCommands (const CommandBuffer& cmdBuffer, float priority)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Check if presentation needs to wait for these commands
	bool addToFrameSync = cmdBuffer.needsFrameSync();
	unsigned subBufIdx =
		  (   addToFrameSync || cmdBuffer.taskClass()==TC::TRANSFER_GFX
		   || (!(cmdBuffer.transient()) && cmdBuffer.taskClass()==TC::TRANSFER))
		? impl.swapchain_curImg : 0;
	VkSemaphore cmdSemaphore = cmdBuffer.semaphore(subBufIdx);
	VkFence cmdFence = cmdBuffer.fence(subBufIdx);
	VkResult result = vkResetFences(impl.logical, 1, &cmdFence);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Context::submitCommands", "Unable to reset command buffer execution fence!"
		));
	bool waitOnSwapchain =
		addToFrameSync && impl.renderSemaphores[impl.swapchain_curFrame].empty();
	if (addToFrameSync)
	{
		impl.renderSemaphores[impl.swapchain_curFrame].push_back(cmdSemaphore);
		impl.swapchainFences[impl.swapchain_curFrame].push_back(cmdFence);
	}

	// Fill out submit info
	VkSemaphore signalSemaphores[] = {cmdSemaphore};
	VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
	VkCommandBuffer cmdBufs[] = {cmdBuffer.handle(subBufIdx)};
	VkSubmitInfo si = {};
	si.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	si.waitSemaphoreCount = waitOnSwapchain ? 1 : 0;
	si.pWaitSemaphores = waitOnSwapchain ?
		&(impl.swapchainSemaphores[impl.swapchain_curFrame]) : nullptr;
	si.pWaitDstStageMask = waitOnSwapchain ? waitStages : 0;
	si.signalSemaphoreCount = addToFrameSync ? 1 : 0;
	si.pSignalSemaphores = addToFrameSync ? signalSemaphores : nullptr;
	si.commandBufferCount = 1;
	si.pCommandBuffers = cmdBufs;
	result = vkQueueSubmit(
		impl.phy.qf[cmdBuffer.queueFamilyIndex()].subQueue(priority), 1, &si, cmdFence
	);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Context::submitCommands", "Unable to submit command buffer!"
		));

	// Notify the command buffer of its submission
	cmdBuffer.notifySubmit(subBufIdx);
}

void Context::presentFrame (void)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Prepare presentation info
	VkSwapchainKHR swapchains[] = { impl.surface.swapchain() };
	VkPresentInfoKHR pi = {};
	pi.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	pi.waitSemaphoreCount =
		(uint32_t)impl.renderSemaphores[impl.swapchain_curFrame].size();
	pi.pWaitSemaphores = impl.renderSemaphores[impl.swapchain_curFrame].data();
	pi.swapchainCount = 1;
	pi.pSwapchains = swapchains;
	pi.pImageIndices = &(impl.swapchain_curImg);

	// Issue the presentation command, while accounting for surface invalidation events
	VkResult result =
		vkQueuePresentKHR(impl.phy.primaryPresentationQF().subQueue(1.0f), &pi);
	if (   result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR
	    || impl.surface.dirty())
	{
		// Recreate swapchain
		waitIdle();
		impl.surface.destroySwapchain();
		ScoredSurfaceConfig sc = { };
		if (!impl.scorePresentationSurface(&sc, impl.surface))
			throw err::NotSupportedException(CGVU_LOGMSG(
				"Context::presentFrame", "Device does not support swapchain re-creation"
				" upon surface change!"
			));
		impl.surface.createSwapchain(
			*this, &(sc.caps), &(sc.format), sc.mode, impl.queueSharingInfo
		);
		for (auto sub : impl.eventSubscribers)
			sub->onPresentationSurfaceChanged(*this);
		for (CommandBuffer *buf : impl.static_drawBuffs)
			buf->build();
	}
	else if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Context::presentFrame", "Unable to queue up presentation command!"
		));

	// Update in-flight frame counters
	impl.swapchain_lastFrame = impl.swapchain_curFrame;
	impl.swapchain_curFrame =
		(impl.swapchain_curFrame+1) % impl.surface.getSwapchainLength();
}

void Context::waitIdle (void) const
{
	VkResult result = vkDeviceWaitIdle(CONTEXT_IMPL->logical);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Context::waitIdle", "Cannot wait for the logical device!"
		));
}

Handle Context::handle (void) const
{
	return CONTEXT_IMPL->logical;
}

Handle Context::handlePhy (void) const
{
	return CONTEXT_IMPL->phy.handle;
}

Handle Context::window (void) const
{
	return CONTEXT_IMPL->hWnd;
}

PresentationSurface& Context::surface (void)
{
	return CONTEXT_IMPL->surface;
}

const PresentationSurface& Context::surface (void) const
{
	return CONTEXT_IMPL->surface;
}

Handle Context::getViewport (void) const
{
	return &(PSURFACE_PTR(CONTEXT_IMPL->surface)->viewport);
}

unsigned Context::getViewportWidth (void) const
{
	return unsigned(PSURFACE_PTR(CONTEXT_IMPL->surface)->viewport.width);
}

unsigned Context::getViewportHeight (void) const
{
	return unsigned(PSURFACE_PTR(CONTEXT_IMPL->surface)->viewport.height);
}

unsigned Context::getSwapchainLength (void) const
{
	return CONTEXT_IMPL->surface.getSwapchainLength();
}

unsigned Context::getSwapImageID (void) const
{
	return CONTEXT_IMPL->swapchain_curImg;
}

unsigned Context::getLastSwapImageID (void) const
{
	return CONTEXT_IMPL->swapchain_lastImg;
}

Handle Context::getSwapImage(unsigned id) const
{
    return CONTEXT_IMPL->surface.getSwapchainImage(id);
}

Handle Context::getCurrentSwapImage (void) const
{
    return CONTEXT_IMPL->surface.getSwapchainImage(CONTEXT_IMPL->swapchain_curImg);
}

Handle Context::getLastSwapImage(void) const
{
    return CONTEXT_IMPL->surface.getSwapchainImage(CONTEXT_IMPL->swapchain_lastFrame);
}

unsigned Context::getLastFrameID(void) const
{
	return CONTEXT_IMPL->swapchain_lastFrame;
}

Handle Context::getDeviceInfo (void) const
{
	return &(CONTEXT_IMPL->phy);
}

TexelFormat Context::bestDepthStencilFormat (bool stencilSupport) const
{
	// Private helper functions
	struct _
	{
		inline static bool supported (const VkFormatProperties &fmtProps)
		{
			const bool ret =   fmtProps.optimalTilingFeatures
			                 & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;
			return ret;
		};
	};

	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Check every candidate in order
	// ToDo: pre-perform the searches during initialization and store supported flags in
	//       a lookup table in the physical device info
	if (!stencilSupport)
	{
		auto it = impl.phy.formatSupport.find(VK_FORMAT_X8_D24_UNORM_PACK32);
		if (it != impl.phy.formatSupport.end() && _::supported(it->second))
			return TFmt::D_INT24;
	}
	auto it = impl.phy.formatSupport.find(VK_FORMAT_D24_UNORM_S8_UINT);
	if (it != impl.phy.formatSupport.end() && _::supported(it->second))
		return TFmt::D24S8_INT;
	if (!stencilSupport)
	{
		it = impl.phy.formatSupport.find(VK_FORMAT_D32_SFLOAT);
		if (it != impl.phy.formatSupport.end() && _::supported(it->second))
			return TFmt::D_FLT32;
		it = impl.phy.formatSupport.find(VK_FORMAT_D16_UNORM);
		if (it != impl.phy.formatSupport.end() && _::supported(it->second))
			return TFmt::D_INT16;
	}
	it = impl.phy.formatSupport.find(VK_FORMAT_D16_UNORM_S8_UINT);
	if (it != impl.phy.formatSupport.end() && _::supported(it->second))
		return TFmt::D16S8_INT;

	// Nothing found...
	// ToDo: throwing in a query function when nothing was found is not good design...
	throw NotSupportedException(CGVU_LOGMSG(
		"Context::bestDepthStencilFormat", "No adequate texel format for depth/stencil"
		" images supported by this Vulkan device!"
	));
}

void Context::subscribeEventListener (EventListener* subscriber)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Check if already subscribed
	if (impl.eventSubscribers.find(subscriber) != impl.eventSubscribers.end())
		throw InvalidOpException(CGVU_LOGMSG(
			"Context::subscribeEventListener", "The listener is already subscribed!"
		));

	// Subscribe
	impl.eventSubscribers.emplace(subscriber);
}

void Context::unsubscribeEventListener (EventListener* subscriber)
{
	// Shortcut for saving one indirection
	auto &impl = *CONTEXT_IMPL;

	// Check if already subscribed
	auto it = impl.eventSubscribers.find(subscriber);
	if (it == impl.eventSubscribers.end())
		throw InvalidOpException(CGVU_LOGMSG(
			"Context::unsubscribeEventListener", "Could not find given listener in"
			" subscriber list!"
		));

	// Unsubscribe
	impl.eventSubscribers.erase(it);
}


////
// cgvu::Vulkan

#define KRONOS_VALIDATION_LAYER "VK_LAYER_KHRONOS_validation"

struct Vulkan_impl
{
	bool initialized=false, started=false;

	std::vector<const char*> usedExts, usedLayers;

	VkInstance vulkan = VK_NULL_HANDLE;
	PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT;
	PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT;

	std::vector<Context> contexts;
	Context* primaryCtx = nullptr;

	// Debug callback
	signed dbgLevels[2] = { -1 };
	VkDebugUtilsMessengerEXT debugMessengers[2] = { (VkDebugUtilsMessengerEXT)nullptr };
	struct VulkanCallbackPayload
	{
		Vulkan_impl *vk_impl;
		VulkanCallbackPayload(Vulkan_impl *vk_impl) : vk_impl(vk_impl) {}
	} callbackPayload;
	Vulkan::DebugCallback onVulkanMessage = onVulkanMessage_defaultImpl;

public:

	Vulkan_impl() : callbackPayload(this)
	{}

	// Callbacks for Vulkan logging.
	static void onVulkanMessage_defaultImpl (const std::string &msg, log::Severity severity)
	{
		switch (severity)
		{
			case log::VERBOSE:
			case log::INFO:
				std::cout << CGVU_TXTMSG("Vulkan", msg, severity) <<std::endl<<std::endl;
				break;

			case log::WARNING:
			case log::ERROR:
				std::cerr << CGVU_TXTMSG("Vulkan", msg, severity) <<std::endl<<std::endl;
				break;

			default:
				/* DoNothing() */;
		}
	}
	static VKAPI_ATTR VkBool32 VKAPI_CALL vulkanDebugCallback_Instance (
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData
	)
	{
		// Shorthand to save additional indirections
		auto &impl = *((VulkanCallbackPayload*)pUserData)->vk_impl;

		// Determine severity
		log::Severity severity = determineMessageSeverity(messageSeverity, messageType);

		// Emit if corresponding channel has high enough level enabled
		if (   messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
		    && impl.dbgLevels[(unsigned)Dbg::LOADER_DRIVER] >= severity)
			impl.onVulkanMessage(pCallbackData->pMessage, severity);
		else if (impl.dbgLevels[(unsigned)Dbg::VALIDATION] >= severity)
			impl.onVulkanMessage(pCallbackData->pMessage, severity);

		return VK_FALSE;
	}
	static VKAPI_ATTR VkBool32 VKAPI_CALL vulkanDebugCallback_LoaderDriver (
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData
	)
	{
		// Shorthand to save additional indirections
		auto &impl = *((VulkanCallbackPayload*)pUserData)->vk_impl;

		// Determine severity
		log::Severity severity = determineMessageSeverity(messageSeverity, messageType);

		// Emit if corresponding channel has high enough level enabled
		if (impl.dbgLevels[(unsigned)Dbg::LOADER_DRIVER] >= severity)
			impl.onVulkanMessage(pCallbackData->pMessage, severity);

		return VK_FALSE;
	}
	static VKAPI_ATTR VkBool32 VKAPI_CALL vulkanDebugCallback_Validation (
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData
	)
	{
		// Shorthand to save additional indirections
		auto &impl = *((VulkanCallbackPayload*)pUserData)->vk_impl;

		// Determine severity
		log::Severity severity = determineMessageSeverity(messageSeverity, messageType);

		// Emit if corresponding channel has high enough level enabled
		if (impl.dbgLevels[(unsigned)Dbg::VALIDATION] >= severity)
			impl.onVulkanMessage(pCallbackData->pMessage, severity);

		return VK_FALSE;
	}

	// Debug messenger helpers
	inline static log::Severity determineMessageSeverity(
		VkDebugUtilsMessageSeverityFlagBitsEXT severityFlags,
		VkDebugUtilsMessageTypeFlagsEXT typeFlags
	)
	{
		// Vulkan is incredibly convoluted regarding debug event classification, but this
		// should order them correctly according to CGVu log levels while accounting for
		// all idiosyncracies in the Vulkan specs.
		if (severityFlags & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
			return log::ERROR;
		else if (   severityFlags & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
		         && !(typeFlags & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT))
			return log::WARNING;
		else if (   severityFlags & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
		         && typeFlags & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
			return log::INFO;
		else
			return log::VERBOSE;
	}
	inline static void setMessageFields (
		VkDebugUtilsMessengerCreateInfoEXT *ci, log::Severity severity
	)
	{
		ci->messageSeverity = 0; ci->messageType = 0;
		switch (severity)
		{
			case log::VERBOSE:
				ci->messageSeverity =   VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
				                      | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;

			case log::INFO:
				ci->messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

			case log::WARNING:
				ci->messageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;

			case log::ERROR:
				ci->messageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		}
	}
};
#define VULKAN_IMPL ((Vulkan_impl*)pimpl)

Vulkan::Vulkan() : pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new Vulkan_impl;
}

Vulkan::Vulkan(Vulkan &&other) : pimpl(nullptr)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
}

Vulkan::~Vulkan()
{
	// De-allocate implementation instance
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *VULKAN_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Application can not continue normal operation from here on
			impl.started = false;

			// Destroy all contexts
			impl.contexts.clear();

			// Handles of Debug Utils extension used for Vulkan logging
			for (auto &dbgMessenger : impl.debugMessengers)
				if (dbgMessenger)
				{
					impl.vkDestroyDebugUtilsMessengerEXT(
						impl.vulkan, dbgMessenger, nullptr
					);
					dbgMessenger = (VkDebugUtilsMessengerEXT)nullptr;
				}

			// Vulkan instance
			if (impl.vulkan)
			{
				vkDestroyInstance(impl.vulkan, nullptr);
				impl.vulkan = VK_NULL_HANDLE;
			}

			impl.initialized = false;
		}

		delete VULKAN_IMPL;
	}
}

Vulkan& Vulkan::operator= (Vulkan &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

void Vulkan::setDebugLevel (VulkanDebugChannel channel, log::Severity verbosity)
{
	if (VULKAN_IMPL->initialized)
		throw InvalidOpException(CGVU_LOGMSG(
			"Vulkan::setDebugLevel", "Unable to change Vulkan debug reporting after"
			" initialization!"
		));
	VULKAN_IMPL->dbgLevels[(unsigned)channel] = verbosity;
}

void Vulkan::setDebugCallback (const Vulkan::DebugCallback &callback)
{
	VULKAN_IMPL->onVulkanMessage = callback;
}

void Vulkan::init (const std::string &appName,
                   const std::set<std::string>& reqInstanceExts,
                   const std::set<std::string>& reqInstanceLayers)
{
	// Shortcut for saving one indirection
	auto &impl = *VULKAN_IMPL;


	////
	// Instance setup

	// Extensions
	uint32_t count = 0;
	VkResult result = vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Vulkan::init", "Unable to query the number of Vulkan instance extensions!"
		));
	std::map<std::string, unsigned> exts;
	// - query extension list
	{ std::vector<VkExtensionProperties> exts_raw(count);
	  result = vkEnumerateInstanceExtensionProperties(nullptr, &count, exts_raw.data());
	  if (result != VK_SUCCESS)
	  	throw VulkanException(result, CGVU_LOGMSG(
	  		"Vulkan::init", "Unable to enumerate Vulkan instance extensions!"
	  	));
	  // Convert to map
	  for (const VkExtensionProperties &ext : exts_raw)
		  exts[ext.extensionName] = ext.specVersion; }
	// - report extension list and commit to c-style format used during instance creation
	std::cout << "[Vulkan::init] Available instance extensions:" << std::endl;
	impl.usedExts.reserve(exts.size());
	for (const auto &ext : exts)
	{
		std::cout << "   * " << ext.first << " (version " << ext.second << ")"
		          << std::endl;
		impl.usedExts.push_back(ext.first.c_str());
	}
	std::cout << std::endl;
	// - check presence of required extensions
	std::vector<std::string> notfound; 
	for (const std::string& reqExt : reqInstanceExts)
		if (exts.find(reqExt) == exts.end())
			notfound.emplace_back(reqExt);
	if (!notfound.empty())
	{
		std::stringstream msg;
		msg << "Required Vulkan instance extension not present:"
		    << std::endl;
		for (const std::string &ext : notfound)
			msg << "   * " << ext << std::endl;
		throw err::NotSupportedException(CGVU_LOGMSG("Vulkan::init", msg.str()));
	}

	// Instance layers
	count = 0;
	result = vkEnumerateInstanceLayerProperties(&count, nullptr);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Vulkan::init", "Unable to query the number of available Vulkan instance"
			" layers!"
		));
	std::vector<VkLayerProperties> layers(count);
	result = vkEnumerateInstanceLayerProperties(&count, layers.data());
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Vulkan::init", "Unable to enumerate available Vulkan instance layers!"
		));
	std::cout << "[Vulkan::init] Available instance layers:" << std::endl;
	for (const auto &layer : layers)
		std::cout << "   * " << layer.layerName << std::endl; std::cout << std::endl;
	impl.usedLayers.reserve(reqInstanceLayers.size());
	bool validationNotRequested = true;
	for (const std::string &reqLayer : reqInstanceLayers)
	{
		if (    validationNotRequested
		    && (reqLayer.compare(KRONOS_VALIDATION_LAYER) == 0))
			validationNotRequested = false;
		bool found = false;
		for (const VkLayerProperties &layer : layers)
		{
			if (std::strcmp(reqLayer.c_str(), layer.layerName) == 0)
			{
				found = true;
				impl.usedLayers.push_back(reqLayer.c_str());
				break;
			}
		}
		if (!found)
			notfound.emplace_back(reqLayer);
	}
	if (!notfound.empty())
	{
		std::stringstream msg;
		msg << "[Vulkan::init] Required Vulkan instance layers not present:"
		    << std::endl;
		for (const std::string &layer : notfound)
			msg << "   * " << layer << std::endl;
		throw err::NotSupportedException(CGVU_LOGMSG("Vulkan::init", msg.str()));
	}
	if (impl.dbgLevels[(unsigned)Dbg::VALIDATION] >= 0)
	{
		bool validationNotPresent = true;
		if (validationNotRequested)
			// Request the Khronos validation layer implicitely
			for (const VkLayerProperties &layer : layers)
			{
				if (std::strcmp(KRONOS_VALIDATION_LAYER, layer.layerName) == 0)
				{
					validationNotPresent = false;
					impl.usedLayers.push_back(KRONOS_VALIDATION_LAYER);
					break;
				}
			}
		if (validationNotPresent)
			throw err::NotSupportedException(CGVU_LOGMSG(
				"Vulkan::init", "Debug logging requested that requires validation, but"
				" the Vulkan instance layer " KRONOS_VALIDATION_LAYER " is not present!"
			));
	}

	// Instance creation
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = appName.c_str();
	appInfo.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
	appInfo.pEngineName = "CGVu";
	appInfo.engineVersion = VK_MAKE_VERSION(0, 0, 1);
	appInfo.apiVersion = VK_API_VERSION_1_2;
	VkInstanceCreateInfo createInfo = {};
	VkDebugUtilsMessengerCreateInfoEXT msgCreateInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = (uint32_t)impl.usedExts.size();
	createInfo.ppEnabledExtensionNames = impl.usedExts.data();
	createInfo.enabledLayerCount = (uint32_t)impl.usedLayers.size();
	createInfo.ppEnabledLayerNames = impl.usedLayers.data();
	signed maxDbgLvl = std::max(impl.dbgLevels[(unsigned)Dbg::LOADER_DRIVER],
	                            impl.dbgLevels[(unsigned)Dbg::VALIDATION]);
	if (maxDbgLvl >= 0)
	{
		impl.setMessageFields(&msgCreateInfo, (log::Severity)maxDbgLvl);
		msgCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		msgCreateInfo.messageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
		msgCreateInfo.pUserData = &(impl.callbackPayload);
		msgCreateInfo.pfnUserCallback = impl.vulkanDebugCallback_Instance;
		createInfo.pNext = &msgCreateInfo;
	}
	result = vkCreateInstance(&createInfo, nullptr, &impl.vulkan);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Vulkan::init", "Unable to create Vulkan instance!"
		));
	impl.initialized = true; // We have something to clean up from this point on
	// - logging setup
	if (maxDbgLvl >= 0)
	{
		impl.vkCreateDebugUtilsMessengerEXT =
			(PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
				impl.vulkan, "vkCreateDebugUtilsMessengerEXT"
			);
		if (!impl.vkCreateDebugUtilsMessengerEXT)
			throw VulkanException(VK_RESULT_MAX_ENUM, CGVU_LOGMSG(
				"Vulkan::init", "Unable to interface with Vulkan debug utils extension"
				" - querying entry point for vkCreateDebugUtilsMessengerEXT() failed!"
			));
		impl.vkDestroyDebugUtilsMessengerEXT =
			(PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
				impl.vulkan, "vkDestroyDebugUtilsMessengerEXT"
			);
		if (!impl.vkCreateDebugUtilsMessengerEXT)
			throw VulkanException(VK_RESULT_MAX_ENUM, CGVU_LOGMSG(
				"Vulkan::init", "Unable to interface with Vulkan debug utils extension"
				" - querying entry point for vkDestroyDebugUtilsMessengerEXT() failed!"
			));
	}
	std::cout << "[Vulkan::init] debug level LOADER_DRIVER: "
	          << impl.dbgLevels[(unsigned)Dbg::LOADER_DRIVER] << std::endl;
	std::cout << "[Vulkan::init] debug level VALIDATION: "
	          << impl.dbgLevels[(unsigned)Dbg::VALIDATION] << std::endl << std::endl;
	if (impl.dbgLevels[(unsigned)Dbg::LOADER_DRIVER] >= 0)
	{
		impl.setMessageFields(
			&msgCreateInfo, (log::Severity)impl.dbgLevels[(unsigned)Dbg::LOADER_DRIVER]
		);
		msgCreateInfo.messageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
		msgCreateInfo.pfnUserCallback = impl.vulkanDebugCallback_LoaderDriver;
		result = impl.vkCreateDebugUtilsMessengerEXT(
			impl.vulkan, &msgCreateInfo, nullptr,
			&impl.debugMessengers[(unsigned)Dbg::LOADER_DRIVER]
		);
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"Vulkan::init", "Unable to create Vulkan debug messenger for channel"
				" LOADER_DRIVER!"
			));
	}
	if (impl.dbgLevels[(unsigned)Dbg::VALIDATION] >= 0)
	{
		impl.setMessageFields(
			&msgCreateInfo, (log::Severity)impl.dbgLevels[(unsigned)Dbg::VALIDATION]
		);
		msgCreateInfo.messageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
		msgCreateInfo.pfnUserCallback = impl.vulkanDebugCallback_Validation;
		result = impl.vkCreateDebugUtilsMessengerEXT(
			impl.vulkan, &msgCreateInfo, nullptr,
			&impl.debugMessengers[(unsigned)Dbg::VALIDATION]
		);
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"Vulkan::init", "Unable to create Vulkan debug messenger for channel"
				" VALIDATION!"
			));
	}

	// Enumerate physical Vulkan devices
	// - query database
	result = vkEnumeratePhysicalDevices(impl.vulkan, &count, nullptr);
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Vulkan::init", "Unable to query the number of physical devices available in"
			"the Vulkan instance!"
		));
	if (!count)
		throw NotSupportedException(CGVU_LOGMSG(
			"Vulkan::init", "The system does not provide any Vulkan-compatible devices!"
		));
	std::vector<VkPhysicalDevice> devices(count);
	result = vkEnumeratePhysicalDevices(impl.vulkan, &count, devices.data());
	if (result != VK_SUCCESS)
		throw VulkanException(result, CGVU_LOGMSG(
			"Vulkan::init", "Unable to enumerate available physical Vulkan devices!"
		));
	// - build internal representation
	for (unsigned i=0; i<devices.size(); i++)
	{
		// General info
		impl.contexts.emplace_back(Context(*this, devices[i]));
		VulkanDeviceInfo &dev = CONTEXT_PTR(impl.contexts.back())->phy;
		vkGetPhysicalDeviceProperties(dev.handle, &(dev.props));
		vkGetPhysicalDeviceFeatures(dev.handle, &(dev.features));
		for (unsigned fmt=0; fmt<=VK_FORMAT_ASTC_12x12_SRGB_BLOCK; fmt++)
		{
			VkFormatProperties fmtProps;
			vkGetPhysicalDeviceFormatProperties(dev.handle, (VkFormat)fmt, &fmtProps);
			if (   fmtProps.bufferFeatures || fmtProps.linearTilingFeatures
			    || fmtProps.optimalTilingFeatures)
				dev.formatSupport.emplace((VkFormat)fmt, std::move(fmtProps));
		}
		vkGetPhysicalDeviceMemoryProperties(dev.handle, &(dev.memProps));

		// Extensions
		result = vkEnumerateDeviceExtensionProperties(dev.handle, nullptr, &count, nullptr);
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"Vulkan::init", "Unable to query the number of extensions supported by"
				" the device!"
			));
		std::vector<VkExtensionProperties> devExts(count);
		result = vkEnumerateDeviceExtensionProperties(dev.handle,nullptr, &count, devExts.data());
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"Vulkan::init", "Unable to query the device extensions!"
			));
		for (const VkExtensionProperties &prop : devExts)
			dev.extensions[prop.extensionName] = prop.specVersion;

		// Queue families
		vkGetPhysicalDeviceQueueFamilyProperties(dev.handle, &count, nullptr);
		std::vector<VkQueueFamilyProperties> qfProps(count);
		vkGetPhysicalDeviceQueueFamilyProperties(dev.handle, &count, qfProps.data());
		for (unsigned q=0; q<count; q++)
		{
			dev.qf.emplace_back(qfProps[q]);
			VulkanQueue &queue = dev.qf.back();

			if (queue.props.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				if (   dev.primaryGraphics < 0
				    || queue.props.queueCount > dev.primaryGraphicsQF().props.queueCount)
					dev.primaryGraphics = (signed)q;
			}
			if (queue.props.queueFlags & VK_QUEUE_COMPUTE_BIT)
			{
				if (   dev.primaryCompute < 0
				    || queue.props.queueCount > dev.primaryComputeQF().props.queueCount)
					dev.primaryCompute = (signed)q;
			}
			if (queue.props.queueFlags & VK_QUEUE_TRANSFER_BIT)
			{
				if (   dev.primaryTransfer < 0
				    || determineDedicatedTransfer(queue)
				    || (   (!determineDedicatedTransfer(dev.primaryTransferQF()))
				        && (  queue.props.queueCount
				            > dev.primaryTransferQF().props.queueCount)))
					dev.primaryTransfer = (signed)q;
			}
		}
	}

	// Log results
	std::cout << "[Vulkan::init] Available Vulkan devices:";
	for (Context &ctx : impl.contexts)
	{
		VulkanDeviceInfo &dev = CONTEXT_PTR(ctx)->phy;
		std::cout << std::endl
		          << "   * ["<< dev.handle << " : ID_" << dev.props.deviceID <<"] "
		          << dev.props.deviceName
		          << " ("<< vulkanDeviceTypeToString(dev.props.deviceType) <<")";
	}
	std::cout << std::endl << std::endl;

	// All done!
	impl.started = true;
}

Handle Vulkan::handle (void)
{
	return VULKAN_IMPL->vulkan;
}

inline static bool findScore (
	float *score, const VulkanDeviceScoreSet &scoring, VulkanScoringCriterion crit
)
{
	const auto &it = scoring.find(crit);
	if (it != scoring.end())
	{
		*score = it->second;
		return true;
	}
	return false;
}

Context& Vulkan::obtainContext (
	Handle window, const VulkanDeviceScoreSet &scoring,
	const std::map<std::string, Vulkan::DeviceExtensionInfo> &reqDeviceExts
)
{
	////
	// Preliminaries

	// Method-wide variables
	VkResult result;
	auto &impl = *VULKAN_IMPL;

	// RAII helper for dealing with incomplete context creation
	// ToDo: add suitable non-templated RAII class to utils
	struct SurfaceHelper {
		PresentationSurface surface;
		VkSurfaceKHR surface_handle = VK_NULL_HANDLE;
		SurfaceHelper(Vulkan &vulkan) : surface(vulkan) {}
	} sh(*this);

	// Special handling for contexts that present to a window
	if (window)
	{
		// First check if the window, if drawing to a window is requested, already has a
		// context associated with it
		for (Context &ctx : impl.contexts)
			if (CONTEXT_PTR(ctx)->hWnd == window)
				// Just return that one
				return ctx;

		// Otherwise create a presentation surface to check the candidate devices against
		sh.surface.init(window);
		sh.surface_handle = sh.surface.handle();
	}


	////
	// Device selection

	// Score physical devices
	uint32_t count = 0;
	std::multimap<float, std::pair<Context&, ScoredSurfaceConfig&> > scoredContexts;
	std::vector<ScoredSurfaceConfig> surfaceInfos(impl.contexts.size());
	for (unsigned i=0; i<surfaceInfos.size(); i++)
	{
		// Shorthands
		Context &ctx = impl.contexts[i];
		ScoredSurfaceConfig &si = surfaceInfos[i];

		// Validate current context usage
		if (CONTEXT_PTR(ctx)->started && window)
			// Context already in use by another window - we know that because if a
			// context for the same window was requested, we would have detected that
			// above
			continue;

		// Init
		float score = 0, finalScore = 0;
		VulkanDeviceInfo &dev = CONTEXT_PTR(ctx)->phy;

		// Check presence of required device extensions
		std::vector<std::pair<std::string, DeviceExtensionInfo> > notfound;
		for (const auto &reqExt : reqDeviceExts)
		{
			auto it = dev.extensions.find(reqExt.first);
			if (    it == dev.extensions.end()
			    || (it->second > 0 && it->second < reqExt.second.version))
				notfound.emplace_back(reqExt);
		}
		if (!notfound.empty())
		{
			// We can't use this device
			std::cout << "[Vulkan::obtainContext] Not considering device "
			             "ID_"<<dev.props.deviceID<<" ("<<dev.props.deviceName<<") - "
			          << " required device extension not supported:" << std::endl;
			for (const auto &ext : notfound)
				std::cout << "   * "<<ext.first<<", version "<<ext.second.version << std::endl;
			continue;
		}

		// Extensions
		if (findScore(&score, scoring, DevScore::PER_EXTENSION))
			finalScore += score * float(dev.extensions.size());

		// Extensions
		if (findScore(&score, scoring, DevScore::PER_QUEUE_FAMILY))
			finalScore += score * float(dev.qf.size());

		// Queue families
		unsigned num_graphics_queues = 0, num_compute_queues = 0;
		for (const VulkanQueue &queue : dev.qf)
		{
			if (queue.props.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				num_graphics_queues += queue.props.queueCount;
			if (queue.props.queueFlags & VK_QUEUE_COMPUTE_BIT)
				num_compute_queues += queue.props.queueCount;
		}
		if (findScore(&score, scoring, DevScore::PER_GRAPHICS_QUEUE))
			finalScore += score * float(num_graphics_queues);
		if (findScore(&score, scoring, DevScore::PER_COMPUTE_QUEUE))
			finalScore += score * float(num_compute_queues);

		// Swapchain support if rendering to window is requested
		bool unifiedPresentation=false;
		if (window)
		{
			// Determine if there is a suitable presentation queue for the given surface
			for (unsigned q=0; q<dev.qf.size(); q++)
			{
				VulkanQueue &queue = dev.qf[q];
				VkBool32 canPresent = false;
				result = vkGetPhysicalDeviceSurfaceSupportKHR(
					dev.handle, q, sh.surface_handle, &canPresent
				);
				if (result != VK_SUCCESS)
					throw VulkanException(result, CGVU_LOGMSG(
						"Vulkan::obtainContext", "Unable to query presentation support"
						" from Vulkan device queue family!"
					));
				// Update choice of presentation properties for this context, regardless
				// of whether it will end up being used or not (ToDo: not ideal, should
				// be done differently)
				if (canPresent)
				{
					queue.canPresent = true;
					// Presentation queue family selection criteria:
					if (// (a) no queue selected yet, or...
					    dev.primaryPresent < 0 ||
					    // (b) it's also the primary graphics queue, or...
					    q == (signed)dev.primaryGraphics ||
					    // (c) the primary graphics queue doesn't support presenting, and
					    //     this presentation queue has the highest capacity
					    (   (dev.primaryPresent != dev.primaryGraphics)
					     && (  queue.props.queueCount
					         > dev.primaryPresentationQF().props.queueCount)))
						// --> select it!
						dev.primaryPresent = (signed)q;
				}
			}
			unifiedPresentation = dev.primaryPresent == dev.primaryGraphics;

			// Score device presentation capability
			if (   (   dev.extensions.find(VK_KHR_SWAPCHAIN_EXTENSION_NAME)
			        == dev.extensions.end())
			    || !(CONTEXT_PTR(ctx)->scorePresentationSurface(&si, sh.surface)))
				// Cannot use this device
				continue;
		}

		// Boost score for agreeable swapchain formats and modes
		float majorContrib = finalScore;
		if (   window // Only in case we actually requested window drawing
		    && findScore(&score, scoring, DevScore::SWAPCHAIN_SUITABILITY))
			finalScore += majorContrib * score*(si.formatScore+si.modeScore);
		// Boost score if a dedicated transfer queue is available
		if (   findScore(&score, scoring, DevScore::DEDICATED_TRANSFER)
		    && determineDedicatedTransfer(dev.primaryTransferQF()))
			finalScore += majorContrib * score;
		// Boost score if graphics queue == presentation queue
		if (   findScore(&score, scoring, DevScore::UNIFIED_PRESENTATION)
		    && unifiedPresentation)
			finalScore += majorContrib * score;
		// Boost score if device is compute-capable
		if (   findScore(&score, scoring, DevScore::COMPUTE_CAPABLE)
		    && num_compute_queues)
			finalScore += majorContrib * score;

		// Boost score further if device is a dedicated GPU
		if (   findScore(&score, scoring, DevScore::IS_DEDICATED)
		    && dev.props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			finalScore *= score;
		// Boost score further if device is an integrated GPU
		if (   findScore(&score, scoring, DevScore::IS_INTEGRATED)
		    && dev.props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
			finalScore *= score;
		// Boost score further if device is a software renderer
		if (   findScore(&score, scoring, DevScore::IS_SOFTWARE)
		    && dev.props.deviceType == VK_PHYSICAL_DEVICE_TYPE_CPU)
			finalScore *= score;

		// Commit to list of candidates
		scoredContexts.emplace(
			finalScore, std::move(std::pair<Context&, ScoredSurfaceConfig&>(ctx, si))
		);
	}

	// Select context of device with highest score
	if (scoredContexts.empty())
		throw NotSupportedException(CGVU_LOGMSG(
			"Vulkan::obtainContext", "No Vulkan device present in the system meets the"
			" specified requirements!"
		));
	auto selected_it = scoredContexts.rbegin();

	// Create logical device if not created already
	Context &selected = selected_it->second.first;
	auto &selected_impl = *CONTEXT_PTR(selected);
	ScoredSurfaceConfig &selected_si = selected_it->second.second;
	VulkanDeviceInfo &dev = selected_impl.phy;

	// Log choice
	std::cout << "[Vulkan::init] Selecting Vulkan device:" << std::endl
	          << "   * [ID_"<<dev.props.deviceID<<"] "<<dev.props.deviceName << std::endl
	          << "     with score="<<selected_it->first << std::endl << std::endl;

	// Init context
	if (!selected_impl.started)
	{
		// Commit scoring criteria
		selected_impl.scoring = scoring;

		// Compile extension list
        // Don't use any of these old extensions that now have a KHR equivalent
        static const std::set<std::string> deprecated = {"VK_EXT_buffer_device_address"};
        // ToDo: Find out just what exactly the deal is with these strange
        //       validation layer breaking extensions
        static const std::set<std::string> blacklist = {
            "VK_NVX_binary_import",
            "VK_NVX_device_generated_commands",
            "VK_EXT_custom_border_color",
            "VK_EXT_provoking_vertex",
            "VK_EXT_pipeline_creation_cache_control",
            "VK_KHR_copy_commands2",
            "VK_NV_cuda_kernel_launch",
            "VK_NV_fragment_shading_rate_enums",
            "VK_NV_present_barrier",
            "VK_NV_win32_keyed_mutex"};

        using ext_pair_t = std::pair<std::string, unsigned int>;
        std::vector<ext_pair_t> enabled_exts;
        std::vector<ext_pair_t> disabled_exts;

        std::partition_copy(std::cbegin(dev.extensions), std::cend(dev.extensions),
                            std::back_inserter(enabled_exts),
                            std::back_inserter(disabled_exts), [](const ext_pair_t &p) {
                                return deprecated.find(p.first) == std::cend(deprecated) &&
                                        blacklist.find(p.first) == std::cend(blacklist);
                            });

        std::vector<const char *> devExts;
        std::transform(std::cbegin(enabled_exts), std::cend(enabled_exts),
                        std::back_inserter(devExts),
                        [](const ext_pair_t &p) { return p.first.c_str(); });

        if (!(disabled_exts.empty()))
        {
            std::cout << "[Vulkan::obtainContext] Disabling the following device"
                            " extensions due to incompatibility with the configuration:"
                        << std::endl;
            for (const auto &[ext, rev] : disabled_exts)
                std::cout << "   * " << ext << std::endl;
            std::cout << std::endl;
        }

		// Setup logical device parameters
		// - general setup
		static const float _1 = 1.0f;
		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos(dev.qf.size());
		std::vector<float> qPriorities;
		for (unsigned q=0; q<queueCreateInfos.size(); q++)
		{
			queueCreateInfos[q].queueCount = dev.qf[q].props.queueCount;
			dev.qf[q].subQueues.resize(queueCreateInfos[q].queueCount);
			qPriorities.resize(queueCreateInfos[q].queueCount);
			for (unsigned sq=0; sq<queueCreateInfos[q].queueCount; sq++)
				qPriorities[sq] = 1.0f - float(sq)/float(qPriorities.size());
			queueCreateInfos[q].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfos[q].queueFamilyIndex = q;
			queueCreateInfos[q].pQueuePriorities = qPriorities.data();
			queueCreateInfos[q].flags = 0;
		}
		VkDeviceCreateInfo devCreateInfo = {};
		devCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		devCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
		devCreateInfo.queueCreateInfoCount = (unsigned)queueCreateInfos.size();
		devCreateInfo.pEnabledFeatures = &(selected_impl.phy.features);
		devCreateInfo.enabledExtensionCount = (unsigned)devExts.size();
		devCreateInfo.ppEnabledExtensionNames = devExts.data();
		// - requested extensions
		auto it=reqDeviceExts.begin();
		if (it!=reqDeviceExts.end())
		{
			struct DevExtFeatureNode {
				VkStructureType sType;
				void* pNext;
			} *devExtFeatureNode = (DevExtFeatureNode*)it->second.features;
			devCreateInfo.pNext = devExtFeatureNode;
			devExtFeatureNode->pNext = nullptr;
			it++;
			for (; it!=reqDeviceExts.end(); it++)
			{
				DevExtFeatureNode *last = devExtFeatureNode;
				devExtFeatureNode = (DevExtFeatureNode*)it->second.features;
				devExtFeatureNode->pNext = nullptr;
				last->pNext = devExtFeatureNode;
			}
		}

		// Create logical device context
		result = vkCreateDevice(
			dev.handle, &devCreateInfo, nullptr, &selected_impl.logical
		);
		if (result != VK_SUCCESS)
			throw VulkanException(result, CGVU_LOGMSG(
				"Vulkan::obtainContext", "Unable to create Vulkan logical device!"
			));
		selected_impl.initialized = true; // This context has something to cleanup now

		// Retrieve queue handles, create command pools, and build swapchain sharing info
		// in the process
		std::vector<uint32_t> srfSharingQueues;
		for (unsigned q=0; q<dev.qf.size(); q++)
		{
			// Queue family setup
			for (unsigned sq=0; sq<dev.qf[q].props.queueCount; sq++)
			{
				vkGetDeviceQueue(selected_impl.logical, q, sq, &dev.qf[q].subQueues[sq]);
				if (!(dev.qf[q].subQueues[sq]))
					throw VulkanException(VK_RESULT_MAX_ENUM, CGVU_LOGMSG(
						"Vulkan::obtainContext", "Unable to retrieve all sub queue"
						" handles in a device queue family!"
					));
			}
			if (   dev.qf[q].canPresent
			    || dev.qf[q].props.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				srfSharingQueues.push_back(q);

			// Command pool creation
			// - common init
			VkCommandPoolCreateInfo cpCreateInfo = {};
			cpCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			cpCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
			cpCreateInfo.queueFamilyIndex = q;
			// - static command pool
			result = vkCreateCommandPool(
				selected_impl.logical, &cpCreateInfo, nullptr, &dev.qf[q].pool_static
			);
			if (result != VK_SUCCESS)
				throw VulkanException(result, CGVU_LOGMSG(
					"Vulkan::obtainContext", "Unable to create normal command pool for"
					" a device queue family!"
				));
			// - transient command pool
			cpCreateInfo.flags |= VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
			result = vkCreateCommandPool(
				selected_impl.logical, &cpCreateInfo, nullptr, &dev.qf[q].pool_transient
			);
			if (result != VK_SUCCESS)
				throw VulkanException(result, CGVU_LOGMSG(
					"Vulkan::obtainContext", "Unable to create transient command pool"
					" for a device queue family!"
				));
		}

		// Finalize presentation surface if we have one
		if (sh.surface_handle)
		{
			// Setup swapchain
			sh.surface.createSwapchain(
				selected_it->second.first, &(selected_si.caps), &(selected_si.format),
				selected_si.mode, srfSharingQueues
			);
			selected_impl.queueSharingInfo = std::move(srfSharingQueues);
			unsigned scl = sh.surface.getSwapchainLength();
			selected_impl.swapchainSemaphores.reserve(scl);
			for (unsigned i=0; i<scl; i++)
				selected_impl.swapchainSemaphores.emplace_back(
					selected.createSemaphore()
				);
			selected_impl.swapchainFences.resize(scl);
			selected_impl.renderSemaphores.resize(scl);
			selected_impl.swapchain_lastFrame = selected_impl.swapchain_curFrame = 0;

			// Transfer ownership to the newly started context
			selected_impl.surface = std::move(sh.surface);
		}

		// Ready to go!
		selected_impl.started = true;
	}

	// All done!
	return selected_it->second.first;
}

const VulkanDeviceScoreSet& Vulkan::getDefaultScoreSet (DefaultScoreSet set)
{
	switch (set)
	{
		case DSS::BEST: return ssBestDevice;
		case DSS::ENERGY_SAVER: return ssEnergySaferDevice;
		case DSS::SOFTWARE: return ssSoftwareDevice;
		default:
			// This should not be happening...
			throw err::InvalidOpException(CGVU_LOGMSG(
				"Vulkan::getDefaultScoreSet", "Unknown default score set requested!"
			));
	}
}
