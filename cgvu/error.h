
#ifndef __ERROR_H__
#define __ERROR_H__


//////
//
// Includes
//

// C++ STL
#include <string>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "log/logging.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {

// Module namespace
namespace err {



//////
//
// Interface definitions
//

// Disable warnings related to unexported base classes
#ifdef _MSC_VER
	#pragma warning (push)
	#pragma warning (disable : 4275)
#endif

/** @brief Base exception type used by CGVu. */
class CGVU_API Exception : public std::exception
{

private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Methods

	/**
	 * @brief
	 *		Format an error string verbalizing the exception type and additional info
	 *		like the underlying error code, if any.
	 */
	virtual std::string getDescription (void) const = 0;


public:

	////
	// Object construction / destruction

	/**
	 * @brief Constructs the exception with a copy of the given event source descriptor.
	 */
	Exception(const log::EventSource &source) noexcept;

	/**
	 * @brief
	 *		Constructs the exception by with the given event source descriptor, moving
	 *		any associated memory into the ownership of the exception.
	 */
	Exception(log::EventSource &&source) noexcept;

	/** @brief Virtual base destructor. Causes vtable creation. */
	virtual ~Exception() noexcept;


	////
	// Interface: std::exception

	/** @brief Returns a string describing the error that caused the exception. */
	const char* what (void) const noexcept;


	////
	// Accessors

	const log::EventSource& eventSource (void) const;
};

/**
 * @brief
 *		Generic exception indicating that some method or functionality was used in an
 *		incorrect way.
 */
class CGVU_API InvalidOpException : public Exception
{

protected:

	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription (void) const;


public:

	////
	// Object construction / destruction

	/** @brief See @ref cgvu::err::Exception::Exception(const log::EventSource &) . */
	InvalidOpException(const log::EventSource &source) noexcept;

	/** @brief See @ref cgvu::err::Exception::Exception(log::EventSource &&) . */
	InvalidOpException(log::EventSource &&source) noexcept;
};


/**
 * @brief
 *		Generic exception indicating that some invoked functionality or parameter(s)
 *		is/are not supported.
 */
class CGVU_API NotSupportedException : public Exception
{

protected:

	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription(void) const;


public:

	////
	// Object construction / destruction

	/** @brief See @ref cgvu::err::Exception::Exception(const log::EventSource &) . */
	NotSupportedException(const log::EventSource& source) noexcept;

	/** @brief See @ref cgvu::err::Exception::Exception(log::EventSource &&) . */
	NotSupportedException(log::EventSource&& source) noexcept;
};


/** @brief Specific exception indicating that a file could not be opened. */
class CGVU_API FileOpenException : public Exception
{

public:

	////
	// Exported types

	/** @brief The access mode that the file was attempted to be opened for. */
	enum AccessType
	{
		READ, WRITE, READWRITE
	};


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription (void) const;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the exception indicating the given access type to the given file. For
	 *		further semantics, see @ref
	 *		cgvu::err::Exception::Exception(const log::EventSource&).
	 */
	FileOpenException(
		AccessType type, const std::string &filename, const log::EventSource &source
	) noexcept;

	/**
	 * @brief
	 *		Constructs the exception indicating the given access type to the given file. For
	 *		further semantics, see @ref cgvu::err::Exception::Exception(log::EventSource&&).
	 */
	FileOpenException(
		AccessType type, const std::string &filename, log::EventSource &&source
	) noexcept;

	/** @brief The destructor. */
	~FileOpenException() noexcept;
};

// Reset to previous warning settings
#ifdef _MSC_VER
	#pragma warning (pop)
#endif



//////
//
// Namespace closings
//

// Module namespace: err
}

// Root namespace: cgvu
}


#endif // ifndef __ERROR_H__
