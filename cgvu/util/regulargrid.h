
#ifndef __REGULARGRID_H__
#define __REGULARGRID_H__


//////
//
// Includes
//

// C++ STL
#include <functional>

// GLM library
#include <glm/glm.hpp>

// Library config
#include "cgvu/libconfig.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {

// Module namespace
namespace util {



//////
//
// Classes
//

/**
 * @brief
 *		RAII wrapper to make dealing with C-style handles and similar things that are
 *		hard to manage using STL-provided facilites (like unique_ptr) exception safe.
 */
template <class FltType>
class CGVU_API Grid3D
{

public:

	////
	// Exported types

	/** @brief Real number type. */
	typedef FltType Real;

	/** @brief 3D-Vector type. */
	typedef glm::vec<3, Real> Vec3;

	/** @brief 4D-Vector type. */
	typedef glm::vec<4, Real> Vec4;

	/** @brief Point accessor type */
	typedef std::function<bool(Vec3*, size_t)> PointAccessor;

	/** @brief Grid cell functionality wrapper. */
	struct Cell
	{
		/** @brief STL-compliant hash functor for grid cells. */
		struct Hash
		{
			/**
			 * @brief
			 *		Uses the 3d vector hash function described in "Optimized Spatial
			 *		Hashing for Collision Detection of Deformable Objects" available
			 *		here:
			 *		http://www.beosil.com/download/CollisionDetectionHashing_VMV03.pdf
			 */
			size_t operator() (const Cell &cell) const
			{
				return size_t(
					cell.x*73856093L ^ cell.y*19349663L ^ cell.z*83492791L
					// ^ mod N is ommitted since it would be (size_t)-1 here, which is
					//   basically a no-op
				);
			}
		};

		/** @brief The @a x grid coordinate of the cell. */
		long long x;

		/** @brief The @a y grid coordinate of the cell. */
		long long y;

		/** @brief The @a z grid coordinate of the cell. */
		long long z;


		////
		// Operators

		/** @brief Equality comparison. */
		inline bool operator == (const Cell &other) const
		{
			return x == other.x && y == other.y && z == other.z;
		}


		////
		// Methods

		/**
		 * @brief
		 *		Determines the cell a given position lies in given a certain grid cell
		 *		width.
		 */
		inline static Cell get (const Vec3 &position, Real cellwidth)
		{
			bool nx = position.x<0, ny = position.y<0, nz = position.z<0;
			return {(long long)(position.x / cellwidth) - long(nx),
			        (long long)(position.y / cellwidth) - long(ny),
			        (long long)(position.z / cellwidth) - long(nz)};
		}
	};


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the grid with the specified cell width, optionally also specifying
	 *		the point accessor. If no point accessor is specified, it needs to be set
	 *		later on using @ref #setPointAccessor .
	 */
	Grid3D(Real cellwidth, PointAccessor pointAccessor=PointAccessor());

	/** @brief The destructor. */
	virtual ~Grid3D();


	////
	// Methods

	/** @brief Inserts the point at the given access index into the grid. */
	void insert (size_t index);

	/**
	 * @brief
	 *		Collects all currently inserted points within the 27-neighborhood of the cell
	 *		that the query point falls in. By default, the points will be reported in
	 *		ascending order of their distance from the query point.
	 */
	bool query (std::vector<size_t> *out, const Vec3 &queryPoint,
	            bool distanceSort=true) const;

	/** @brief Sets the point accessor used to retrieve actual point data. */
	void setPointAccessor (PointAccessor pointAccessor);
};



//////
//
// Namespace closings
//

// Module namespace: util
}

// Root namespace: cgvu
}


#endif // ifndef __REGULARGRID_H__
