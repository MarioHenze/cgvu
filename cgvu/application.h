
#ifndef __APPLICATION_H__
#define __APPLICATION_H__


//////
//
// Includes
//

// C++ STL
#include <vector>
#include <set>
#include <any>
#include <string>
#include <functional>
#include <exception>
#include <stdexcept>

// GLM library
#include <glm/glm.hpp>

// Library config
#include "libconfig.h"

// CGVU includes
#include "error.h"
#include "settings.h"
#include "log/logging.h"
#include "vk/vulkan.h"
#include "vk/texture.h"



//////
//
// Exception declarations
//

// Error namespace
namespace cgvu::err {

/** @brief Indicates an error reported by the GLFW windowing utility library. */
class GLFWException : public Exception
{

private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription (void) const;


public:

	////
	// Object construction / destruction

	/** @brief See @ref cgvu::err::Exception::Exception(const log::EventSource &) . */
	GLFWException(const log::EventSource &source) noexcept;

	/** @brief See @ref cgvu::err::Exception::Exception(log::EventSource &&) . */
	GLFWException(log::EventSource &&source) noexcept;

	/** @brief The destructor. */
	~GLFWException() noexcept;
};

// Close error namespace
}



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Classes in this header
template <class FltType> class ApplicationStub;



//////
//
// Interface definitions
//

/** @brief Camera control abstraction interface. */
template<class FltType>
class CGVU_API Camera
{

public:

	////
	// Exported types

	/** @brief Real number type. */
	typedef FltType Real;

	/** @brief 3D-Vector type. */
	typedef glm::vec<2, Real> Vec2;

	/** @brief 3D-Vector type. */
	typedef glm::vec<3, Real> Vec3;

	/** @brief 4D-Vector type. */
	typedef glm::vec<4, Real> Vec4;

	/** @brief 4x4 Matrix type. */
	typedef glm::mat<4, 4, Real> Mat4;


	////
	// Object construction / destruction

	/** @brief The destructor. */
	virtual ~Camera() {}


	////
	// Methods

	/**
	 * @brief
	 *		Informs the camera about the dimensions of the viewport it represents.
	 *		Implementations may assume that this will be called at least once before any
	 *		call to @ref update .
	 */
	virtual void setViewport (unsigned width, unsigned height) = 0;

	/** @brief Causes the camera to assume a given position and orientation. */
	virtual void lookAtFoV(
		const Vec3 &at, const Vec3 &eyePos, const Vec3 &up, Real FoV
	) = 0;

	/** @brief Causes the camera to update its state. */
	virtual void update (
		const typename ApplicationStub<Real>::Stats &stats,
		const typename ApplicationStub<Real>::MouseState &mouse
	) = 0;

	/** @brief Returns the viewing matrix defined by this camera. */
	virtual const Mat4& viewMatrix (void) const = 0;

	/** @brief Returns the projection matrix defined by this camera. */
	virtual const Mat4& projectionMatrix (void) const = 0;

	/**
	 * @brief Queries the current world-space camera position.
	 *
	 * @return The current eye point, in world space.
	 */
	virtual Vec3 position (void) const = 0;
};

/** @brief Main framework interface, abstracting an application. */
class CGVU_API Application
{

public:

	////
	// Methods

	/**
	 * @brief Handles top-level control flow.
	 *
	 * @param argc
	 *		The number of command line arguments contained in @a argv .
	 * @param argv
	 *		C-style array of @c NULL terminated strings containing the command line
	 *		arguments passed to the application.
	 *
	 * @return The application exit code.
	 */
	virtual int run (int argc, char* argv[]) = 0;
};


/**
 * @brief
 *		Logic stub for implementations of @ref cgvu::Application supporting single or
 *		double precission floating point math.
 */
template <class FltType>
class CGVU_API ApplicationStub : public Application, public Context::EventListener
{

public:

	////
	// Exported types

	/** @brief Real number type. */
	typedef FltType Real;

	/** @brief 2D-Vector type. */
	typedef glm::vec<2, Real> Vec2;

	/** @brief 3D-Vector type. */
	typedef glm::vec<3, Real> Vec3;

	/** @brief 4D-Vector type. */
	typedef glm::vec<4, Real> Vec4;

	/** @brief 2x2 Matrix type. */
	typedef glm::mat<2, 2, Real> Mat2;

	/** @brief 3x3 Matrix type. */
	typedef glm::mat<3, 3, Real> Mat3;

	/** @brief 4x4 Matrix type. */
	typedef glm::mat<4, 4, Real> Mat4;

	/** @brief Stores various application-side statistics about a frame. */
	struct Stats
	{
		/** @brief Overall processing time of the frame, in seconds. */
		Real frameTime;

		/** @brief Current FPS. */
		Real FPS;

		/** @brief Current average FPS. */
		Real avgFPS;
	};

	/** @brief Encapsulates mouse state. */
	struct MouseState
	{
		// Interfacing
		friend class ApplicationStub;

		// Buttons
		bool button [3];
		// Positions and scrolling
		Vec2 curPos, lastPos, movement, scroll;

	private:
		// Scrolling (mouse wheel, pinch gestures, etc.)
		Vec2 scrollState;

	public:
		// Scroll checkers
		inline bool isScrollingX (void) const
		{
			auto &m = *const_cast<MouseState*>(this);
			m.scroll.x = m.scrollState.x;
			m.scrollState.x = 0;
			return m.scroll.x != 0;
		}
		inline bool isScrollingY (void) const
		{
			auto &m = *const_cast<MouseState*>(this);
			m.scroll.y = m.scrollState.y;
			m.scrollState.y = 0;
			return m.scroll.y != 0;
		}
	};

	/** @brief Encapsulates the modelview-projection ransformation stack. */
	class CGVU_API TransformationStack
	{
		// Interfacing
		friend class ApplicationStub;

	private:
		/** @brief Implementation handle. */
		void *pimpl;

	public:
		/** @brief The default constructor. */
		TransformationStack();
		/** @brief Not copy-constructible. */
		TransformationStack(const TransformationStack &) = delete;
		/** @brief The destructor. */
		~TransformationStack();

		/** @brief Not copy-assignable. */
		TransformationStack& operator= (const TransformationStack&) = delete;

		/** @brief Accesses the current (top-of-stack) modelview transformation. */
		Mat4& modelview (void);
		/**
		 * @brief
		 *		Accesses the current (top-of-stack) modelview transformation in read-only
		 *		mode.
		 */
		const Mat4& modelview (void) const;

		/** @brief Accesses the current (top-of-stack) projection matrix. */
		Mat4& projection (void);
		/**
		 * @brief
		 *		Accesses the current (top-of-stack) projection matrix in read-only mode.
		 */
		const Mat4& projection (void) const;

		/**
		 * @brief
		 *		Pushs a duplicate of the current top-of-stack modelview matrix onto the
		 *		stack.
		 */
		void pushModelview (void);

		/** @brief Pops the current top-of-stack modelview matrix from the stack. */
		void popModelview (void);

		/**
		 * @brief
		 *		Pushs a duplicate of the current top-of-stack projection matrix onto the
		 *		stack.
		 */
		void pushProjection (void);

		/** @brief Pops the current top-of-stack projection matrix from the stack. */
		void popProjection (void);

		/**
		 * @brief
		 *		Calculates the inverse transpose of the current (top-of-stack) modelview
		 *		matrix. */
		Mat4 getNormalMat (void) const;
	};


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


	////
	// Methods

	/** @brief Performs generic initialization, dispatches user initializations. */
	void init (void);

	/** @brief Performs generic cleanup, dispatches user cleanup. */
	void cleanup (void);


protected:

	////
	// Object construction / destruction

	/** @brief The default constructor. */
	ApplicationStub();


	////
	// Methods

	/**
	 * @brief
	 *		Sets application main window properties. This may also be called post-init.
	 */
	void configureMainWindow (unsigned width, unsigned height);

	/** @brief Sets up Vulkan logging for the given channel. */
	void setVulkanLogLevel (VulkanDebugChannel channel, log::Severity severity);

	/**
	 * @brief
	 *		Sets guidelines for Vulkan device selection by giving weight to certain
	 *		criteria.
	 */
	void guideVulkanDeviceSelection (VulkanScoringCriterion criterion, float score);

	/** @brief Use the given default scoreset for guiding Vulkan device selection. */
	void guideVulkanDeviceSelection (DefaultScoreSet defaultScoreSet);

	/**
	 * @brief
	 *		Sets the main camera which is responsible for pre-multiplying the modelview
	 *		and projection matrices on the @link #transforms transformation stack
	 *		@endlink .
	 *
	 * @note
	 *		Except for the default camera, the lifetime of which is managed internally,
	 *		The client owns the instance provided to this function and is thus
	 *		responsible for initialization, cleanup etc.
	 *
	 * @param camera
	 *		Instance of a @c Camera object that should control the main application
	 *		viewport.
	 */
	void setMainCamera (Camera<Real> &camera);

	/**
	 * @brief
	 *		References the @c Camera instance that is currently controlling the main
	 *		viewport.
	 *
	 * @return Reference to the current main viewport camera.
	 */
	const Camera<Real>& mainCamera (void) const;


	////
	// Internal interface

	/** @brief Declare required Vulkan instance layers. None if not overriden. */
	virtual void declareRequiredInstanceLayers (std::set<std::string> *out) const;

	/** @brief Declare required Vulkan instance extensions. None if not overriden. */
	virtual void declareRequiredInstanceExtensions (std::set<std::string> *out) const;

	/**
	 * @brief
	 *		Declare required Vulkan device extensions and their versions. None if not
	 *		overriden.
	 */
	virtual void declareRequiredDeviceExtensions (
		std::map<std::string, Vulkan::DeviceExtensionInfo> *out
	) const;

	/** @brief Optional client hook for post-construction time initialization. */
	virtual void mainInit (Context &context);

	/**
	 * @brief Optional client hook for rendering additional GUI elements.
	 *
	 * @note
	 *		As the architecture of the framework gets fleshed out, this API will undergo
	 *		significant changes.
	 */
	virtual void drawGUI (Context &context);

	/** @brief Mandatory client hook implementing the application main loop body. */
	virtual bool mainLoop (Context &context, const Stats &stats) = 0;

	/** @brief Optional client hook for pre-shutdown deinitialization / cleanup. */
	virtual void mainCleanup (Context &context);

	/** @brief Default top-level exception handler. */
	virtual int handleUnhandledException (const std::exception &e);


public:

	////
	// Object construction / destruction

	/** @brief Virtual base destructor. Causes vtable creation. */
	virtual ~ApplicationStub();


	////
	// Methods

	/**
	 * @brief Handles top-level control flow. See @ref cgvu::Application::run .
	 *
	 * @param argc
	 *		See @ref cgvu::Application::run .
	 * @param argv
	 *		See @ref cgvu::Application::run .
	 *
	 * @return See @ref cgvu::Application::run .
	 */
	int run (int argc, char* argv[]);

	/** @brief References the transformation stack. */
	TransformationStack& transforms (void);

	/** @brief Retrieves the mouse state. */
	const MouseState& mouse (void) const;

	/** @brief References the main depth/stencil buffer. */
	const DepthStencilBuffer& mainDepthStencil (void);

	/** @brief References the Vulkan instance used by the application. */
	Vulkan& vulkanInstance (void);

	/**
	 * @brief References the Vulkan instance used by the application (read-only access).
	 */
	const Vulkan& vulkanInstance (void) const;

	/**
	 * @brief
	 *		References the primary Vulkan context used for presenting to the application
	 *		window.
	 */
	Context& primaryContext (void);

	/**
	 * @brief
	 *		References the primary Vulkan context used for presenting to the application
	 *		window (read-only access).
	 */
	const Context& primaryContext (void) const;

	/** @brief Reports the current width of the main window drawing surface. */
	unsigned mainSurfaceWidth (void) const;

	/** @brief Reports the current height of the main window drawing surface. */
	unsigned mainSurfaceHeight (void) const;

	/** @brief Reports the name of the application. */
	virtual std::string getName (void) = 0;

	/**
	 * @brief
	 *		Notified after the main window finished a resize operation, reporting the new
	 *		dimensions of the client area.
	 */
	virtual void onMainWindowResized(unsigned newWidth, unsigned newHeight) {};


	////
	// Interface: cgvu::Context::EventListener

	/** @brief See @ref cgvu::Context::EventListener::onWindowIsResizing . */
	virtual void onWindowIsResizing (
		cgvu::Context &ctx, unsigned newWidth, unsigned newHeight
	);

	/** @brief See @ref cgvu::Context::EventListener::onWindowRefresh . */
	virtual void onWindowRefresh (cgvu::Context &ctx);

	/** @brief See @ref cgvu::Context::EventListener::onPresentationSurfaceChanged . */
	virtual void onPresentationSurfaceChanged (cgvu::Context &ctx);
};



//////
//
// Class definitions
//

/** @brief Camera control providing an orbit view around a focus point. */
template<class FltType>
class CGVU_API OrbitCamera : public Camera<FltType>
{

public:

	////
	// Exported types

	/** @brief Real number type. */
	typedef typename Camera<FltType>::Real Real;

	/** @brief 2D-Vector type. */
	typedef typename Camera<Real>::Vec2 Vec2;

	/** @brief 3D-Vector type. */
	typedef typename Camera<Real>::Vec3 Vec3;

	/** @brief 4D-Vector type. */
	typedef typename Camera<Real>::Vec4 Vec4;

	/** @brief 4x4 Matrix type. */
	typedef typename Camera<Real>::Mat4 Mat4;

	/**
	 * @brief
	 *		Function signature for depth map providers (see @ref #setDepthMapProvider ).
	 */
	typedef std::function<Real(const Vec2 &)> DepthMapProvider;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/** @brief Constructs the camera with the given initial orbit settings. */
	OrbitCamera(
		const Vec3 &initalFocus={0,0,0}, Real initialDist=2, Real initialTheta=0,
		Real initialPhi=0, Real initialFoV=45
	);

	/** @brief Constructs the camera with the given generic view settings. */
	OrbitCamera(const Vec3 &at, const Vec3 &eyePos, Real FoV);

	/** @brief The destructor. */
	virtual ~OrbitCamera();


	////
	//  Interface: cgvu::Camera

	/** @brief See @ref cgvu::Camera::setViewport . */
	virtual void setViewport (unsigned width, unsigned height);

	/** @brief See @ref cgvu::Camera::lookAtFoV . */
	virtual void lookAtFoV (
		const Vec3 &at, const Vec3 &eyePos, const Vec3 &up, Real FoV
	);

	/** @brief See @ref cgvu::Camera::update . */
	virtual void update (
		const typename ApplicationStub<Real>::Stats &stats,
		const typename ApplicationStub<Real>::MouseState &mouse
	);

	/** @brief See @ref cgvu::Camera::viewMatrix . */
	virtual const Mat4& viewMatrix (void) const;

	/** @brief See @ref cgvu::Camera::projectionMatrix . */
	virtual const Mat4& projectionMatrix (void) const;

	/**
	 * @brief Queries the current world-space camera position.
	 *
	 * @return The current eye point, in world space.
	 */
	virtual Vec3 position (void) const;


	////
	//  Methods

	/**
	 * @brief
	 *		Sets the provider of depth information in case doulbe-click-to-focus
	 *		functionality is desired.
	 */
	void setDepthMapProvider (const DepthMapProvider &depthMapProvider);
};



//////
//
// Function definitions
//

/**
 * @brief
 *		Retrieves a pointer to the currently active @link cgvu::Application application
 *		@endlink object.
 */
extern "C" CGVU_API Application* getApplication (void);

/**
 * @brief
 *		Retrieves a reference to the currently active @link cgvu::Application application
 *		@endlink object. Will cause a segmentation fault if no application has been
 /		created yet.
 */
CGVU_API Application& referenceApplication (void);



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __APPLICATION_H__
