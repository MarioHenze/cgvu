
# README #

This README documents gives a (very) brief introduction of the project and details the steps necessary to build and use CGVu.

## The CGVu framework ##

CGVu is a C++ framework for rapid prototyping of Vulkan-based graphics applications, mainly aimed at the computer graphics and visualization research community.

It is currently in the very early stages of development. The API is far from being stable and it is currently *\*not\** recommended for any sort of production use.

## Build instructions ##

### General ###

CGVu uses CMake and is intended to run on any PC-based platform with Vulkan support, although it is primarily being developed on Microsoft Windows and not all OS abstractions are available on other platforms yet.

CGVu makes use of a small number of Free and Open Source 3rd party libraries. These currently need to be installed on the system used for development in a way such that CMake can find them (see *Dependencies* below), although with the exception of the Vulkan SDK, automatic fallback to in-tree versions of them is planned for the future.

### Dependencies ###

- The [Vulkan SDK](https://vulkan.lunarg.com/) version 1.2 or later.
   If CMake cannot find your installation of the SDK, set the environment variables `VK_SDK_PATH` or `VULKAN_SDK` - point at the *root directory* of your installation, e.g. `C:\Libs\Vulkan-v1.2.135.0`
- [GLFW](https://www.glfw.org/) version 3.3 or later.
   If CMake cannot find your installation of GLFW, set the environment variable `GLFW_ROOT_DIR` - point it at the *root directory* of your installation, e.g. `C:\Libs\GLFW-v3.3`
- [GLM](https://glm.g-truc.net/) version 0.9.9 or later
   If CMake cannot find your installation of GLM, set the environment variable `GLM_ROOT_DIR` - point it at the *root directory* of your installation, e.g. `C:\Libs\glm-v0.9.9.6`

### Building CGVu and its examples ###

Normally, you wouldn't build CGVu stand alone. Instead, you would point the build system of your application to a local clone of this repository (e.g. as a git submodule) and build it along with your application. This works best if your application also uses CMake (see next section). However, it is possible to build CGVu by itself and run the example applications.

First, make sure the dependencies are installed. CGVu supports 64 bits only at this point, so make sure the dependencies are 64-bit as well. The following instructions will result in an out-of-tree build, which is the only officially supported way of building CGVu with CMake:

1. Create some directory, e.g. `C:/Projects/CGVu` where all the action will take place, and `cd` into it:
> `c:`  
> `mkdir Projects\CGVu`  
> `cd /Projects/CGVu`

2. Clone the repository in some sub-directory, e.g. `git`:
> `git clone https://bitbucket.org/brussig-tud/cgvu.git ./git`

3. Create a subdirectory for building next to where you clone the repository, e.g. `build`, and `cd` into it:
> `mkdir build`  
> `cd ./build`

4. Configure the build using CMake, and make sure to build the example applications. We use the Visual Studio 2019 generator, which supports different target architectures. To be safe, we tell it to target x64:
> `cmake ../git -G "Visual Studio 16 2019" -A x64 -D CGVU_BUILD_EXAMPLES=ON`

5. This will create the Visual Studio 2019 solution `CGVu.sln` in `C:\Projects\CGVu\build`. You can open and build it like any Visual Studio solution. To run any of the examples, select the corresponding project in the solution explorer and mark it as the startup project via the right-click context menu. F5 or CTRL-F5 will then run the example.

### Building CGVu as part of another project ###

t.b.d.
