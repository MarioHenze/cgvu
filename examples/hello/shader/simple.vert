#version 450
#extension GL_KHR_vulkan_glsl : enable

// Uniforms
layout(binding = 0) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;

// Inputs
layout(location = 0) in vec4 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;
layout(location = 3) in vec3 in_color;

// Outputs
layout(location = 0) out vec3 out_pos;
layout(location = 1) out vec3 out_normal;
layout(location = 2) out vec2 out_texcoord;
layout(location = 3) out vec3 out_color;


// Shader entry point
void main()
{
	// Save eye space position
	vec4 pos_tmp = cgvu_trans.modelview * in_pos;

	// Projection
	gl_Position = cgvu_trans.proj * pos_tmp;

	// Output attributes
	out_pos = pos_tmp.xyz / pos_tmp.w;
	out_normal = normalize((cgvu_trans.normalMat * vec4(in_normal, 0)).xyz);
	out_texcoord = in_texcoord;
	out_color = in_color;
}
