
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <exception>
#include <stdexcept>

// GLM library
#include <glm/gtc/matrix_transform.hpp>

// GLFW library
// - ToDo: abstract away behind framework windowing module to eliminate this include
//         (currently used only for keyboard / mouse input here)
#include <GLFW/glfw3.h>

// ImGUI library
// - ToDo: abstract away behind framework functionality to eliminate this include
#include <imgui.h>

// CGVu library
#include <cgvu/util/utils.h>
#include <cgvu/application.h>
#include <cgvu/vk/buffer.h>
#include <cgvu/vk/texture.h>
#include <cgvu/vk/shaderprog.h>



//////
//
// Setup constants
//

int viewportWidth = 1152;
int viewportHeight = 864;



//////
//
// Module-private classes
//

// Application
class HelloTetrahedron : public cgvu::ApplicationStub<float>
{

public:

	////
	// Exported types

	// Example vertex type
	struct ExampleVertex {
		Vec4 pos;
		Vec3 normal;
		alignas(16) Vec2 texCoord;
		alignas(16) Vec3 color;

		inline static cgvu::BufferBindingDescs& bindingDescs (void)
		{
			static cgvu::BufferBindingDescs bd = {{ 0, sizeof(ExampleVertex), false }};
			return bd;
		}
		inline static cgvu::AttribDescs& attributeDescs (void)
		{
			static cgvu::AttribDescs ad = {
				{ 0, 0, offsetof(ExampleVertex, pos), cgvu::AFmt::VEC4_FLT32 },
				{ 1, 0, offsetof(ExampleVertex, normal), cgvu::AFmt::VEC3_FLT32 },
				{ 2, 0, offsetof(ExampleVertex, texCoord), cgvu::AFmt::VEC2_FLT32 },
				{ 3, 0, offsetof(ExampleVertex, color), cgvu::AFmt::VEC3_FLT32 }
			};
			return ad;
		}
	};

	// Example uniform structs
	struct CGVuTransforms {
		Mat4 modelview;
		Mat4 proj;
		Mat4 normalMat;
	};
	struct CGVuMaterial {
		Vec4 albedo;
		float specularity;
		float shininess;
	};
	struct CGVuGlobalLight {
		Vec3 pos;   // in eye space
		alignas(16) Vec3 color;
	};


protected:

	////
	// Data members

	// Example texture
	cgvu::Texture texture;

	// The example shader and its uniforms
	cgvu::ShaderProgram sh;
	cgvu::UniformBuffer shUniforms;
	cgvu::TypedUniform<CGVuTransforms> shTrans;
	cgvu::TypedUniform<CGVuMaterial> shMat;
	cgvu::TypedUniform<CGVuGlobalLight> shLight;

	// The example pipeline
	cgvu::Pipeline pl;

	// The example command buffer
	cgvu::CommandBuffer cmd;

	// Example geometry (tetrahedron)
	inline static const Real
		sn = glm::sin(glm::radians<Real>(60)), cs = Real(0.5); // <-- cos(60�)
	cgvu::StorageBuffer vertexBuf, indexBuf;
	std::vector<ExampleVertex> vertices = {
		// Front triangle
		{{-sn, -cs, cs, 1}, {}, {-1.0f, 1.5f}, {1.0f, 0.0f, 0.0f}},
		{{ sn, -cs, cs, 1}, {}, { 2.0f, 1.5f}, {0.0f, 0.0f, 1.0f}},
		{{  0,   1,  0, 1}, {}, { 0.5f,-1.5f}, {0.0f, 1.0f, 0.0f}},
		// Right-back triangle
		{{ sn, -cs, cs, 1}, {}, {-1.0f, 1.5f}, {0.0f, 0.0f, 1.0f}},
		{{  0, -cs, -1, 1}, {}, { 2.0f, 1.5f}, {0.7f, 0.5f, 0.0f}},
		{{  0,   1,  0, 1}, {}, { 0.5f,-1.5f}, {0.0f, 1.0f, 0.0f}},
		// left-back triangle
		{{-sn, -cs, cs, 1}, {}, { 2.0f, 1.5f}, {1.0f, 0.0f, 0.0f}},
		{{  0,   1,  0, 1}, {}, { 0.5f,-1.5f}, {0.0f, 1.0f, 0.0f}},
		{{  0, -cs, -1, 1}, {}, {-1.0f, 1.5f}, {0.7f, 0.5f, 0.0f}},
		// bottom triangle
		{{ sn, -cs, cs, 1}, {}, { 2.0f, 1.5f}, {0.0f, 0.0f, 1.0f}},
		{{-sn, -cs, cs, 1}, {}, {-1.0f, 1.5f}, {1.0f, 0.0f, 0.0f}},
		{{  0, -cs, -1, 1}, {}, { 0.5f,-1.5f}, {0.7f, 0.5f, 0.0f}}
	};
	std::vector<unsigned> indices = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

	// State
	Real angle=0;


public:

	////
	// Object construction / destruction

	// Virtual base destructor. Causes vtable creation.
	HelloTetrahedron() : sh(cgvu::SHADER_SRC_SPK, "shader/simple.spk")
	{
		configureMainWindow(viewportWidth, viewportHeight);
		#ifdef _DEBUG
			setVulkanLogLevel(cgvu::Dbg::LOADER_DRIVER, cgvu::log::INFO);
			setVulkanLogLevel(cgvu::Dbg::VALIDATION, cgvu::log::WARNING);
		#else
			setVulkanLogLevel(cgvu::Dbg::LOADER_DRIVER, cgvu::log::WARNING);
		#endif
		//guideVulkanDeviceSelection(cgvu::DSS::ENERGY_SAVER);
	}


	////
	// Interface: cgvu::Application

	// App name
	virtual std::string getName (void)
	{
		return "Hello Tetrahedron";
	}

	// Instance extensions we require
	virtual void declareRequiredInstanceExtensions (std::set<std::string> *out) const
	{
	}

	// Instance layers we require
	virtual void declareRequiredInstanceLayers (std::vector<const char*> *out) const
	{
	}

	// Post-constructor init
	virtual void mainInit (cgvu::Context &ctx)
	{
		// Example texture
		texture = cgvu::Texture(ctx, "../../resources/texture/vulkan.png");
		auto texSampler = ctx.obtainSampler(
			cgvu::SFlt::LINEAR, cgvu::SFlt::LINEAR, cgvu::SRpt::BORDER,
			cgvu::SRpt::BORDER, cgvu::SRpt::BORDER, 4
		);

		// Example shader
		sh.build(ctx, {
			{0, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::VERTEX},
			{1, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::FRAGMENT},
			{2, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::FRAGMENT},
			{3, cgvu::UFrm::SAMPLER, 1, cgvu::SH::FRAGMENT}
		});
		shUniforms = cgvu::UniformBuffer(ctx);
		shTrans = shUniforms.insert<CGVuTransforms>();
		shMat = shUniforms.insert<CGVuMaterial>();
		shLight = shUniforms.insert<CGVuGlobalLight>();
		shUniforms.build();
		sh.updateDescriptorSets({
			cgvu::UniformUpdate(0, shTrans),
			cgvu::UniformUpdate(1, shMat),
			cgvu::UniformUpdate(2, shLight),
			cgvu::SamplerUpdate(3, texture, texSampler)
		});

		// Setup tetrahedron
		// - material
		shMat.initialize({/*albedo*/{1, 1, 1, 1}, /*specularity*/0.5, /*shininess*/96});
		// - finalize normals
		for (unsigned i=0; i<indices.size(); i+=3)
		{
			Vec3 e1 = Vec3(vertices[i+1].pos) - Vec3(vertices[i].pos),
			     e2 = Vec3(vertices[i+2].pos) - Vec3(vertices[i+1].pos),
			     n = glm::normalize(glm::cross(e1, e2));
			vertices[i+2].normal =
				vertices[i+1].normal =
					vertices[i].normal = n;
		}

		// Fill buffers
		vertexBuf = cgvu::StorageBuffer(ctx, cgvu::SBT::VERTEX);
		vertexBuf.upload(vertices);
		indexBuf = cgvu::StorageBuffer(ctx, cgvu::SBT::INDEX);
		indexBuf.upload(indices);

		// Create pipeline
		pl = ctx.createPipeline(sh);
		pl.setInputBindings<ExampleVertex>(cgvu::PT::TRIANGLE_LIST);
		pl.attachDepthStencilBuffer(mainDepthStencil());
		pl.build();

		// Static ommand buffer for drawing
		cmd = ctx.createCommandBuffer(cgvu::TC::RENDER, false);
		cmd.startRecording();
		cmd.bindPipeline(pl);
		cmd.bindBuffer(ExampleVertex::bindingDescs()[0].binding, vertexBuf);
		cmd.bindIndexBuffer(indexBuf);
		cmd.drawIndexed(indices.size(), 1, 0);
		cmd.build();

		// Attach light to camera (by specifying position in eye coordinates)
		shLight.initialize({/*pos*/{0.125, 0.0625, 0}, /*color*/{1, 1, 1}});
	}

	// GUI drawing
	void drawGUI (cgvu::Context &context)
	{
		// Just the ImGUI demo for now
		ImGui::ShowDemoWindow();
	}

	// Main loop body
	virtual bool mainLoop (cgvu::Context &ctx, const Stats &stats)
	{
		// Update State
		angle = glm::mod<Real>(angle + 45*stats.frameTime, 360);

		// Handle input
		auto &io = ImGui::GetIO();

		// Apply node transformation
		auto &transf = transforms();
		transf.pushModelview();
		transf.modelview() = glm::rotate(
			transf.modelview(), (float)glm::radians(angle), {0, 1, 0}
		);

		// Update uniforms
		auto &trans = shTrans.set();
		trans.modelview = transf.modelview();
		trans.proj = transf.projection();
		trans.normalMat = transf.getNormalMat();

		// Render
		ctx.submitCommands(cmd);

		// Reset our changes to the transformation stack
		transf.popModelview();

		// Shut down on ESC key
		return !(io.WantCaptureKeyboard) && ImGui::IsKeyPressed(GLFW_KEY_ESCAPE, false);
	}
};



//////
//
// Application entry point
//

int main (int argc, char* argv[])
{
	// Create application object
	HelloTetrahedron helloApp;

	// Hand off control flow
	return helloApp.run(argc, argv);
}
